# -*- coding: utf-8 -*-
from itsdangerous import (TimedJSONWebSignatureSerializer, SignatureExpired,
                          BadSignature)
from .exceptions import Forbidden
from . import settings

"""
Generate tokens for request validation
"""


class TokenType(object):
    """Token types

    Attributes
    ----------
    SignUp: str
        Sign up token with value 'signup'
    RestorePassword: str
        Restore password token with value 'restore_password'
    """
    SignUp = 'signup'
    RestorePassword = 'restore_password'


def generate(expires=900, **kwargs):
    """Generate a token

    Parameters
    ----------
    expires: int, optional
        Number of seconds for which the token will be valid. Defaults to 900
    **kwargs: dict
        Data to be included in the token. Do not include 'expires' or
        'date' as those keys are used to check expiration

    Returns
    -------
    str
        A token

    Raises
    ------
    ValueError
        If TOKEN_SECRET is not set (use krieger_gae.set_token_secret)
    """
    TOKEN_SECRET = settings('TOKEN_SECRET')

    if TOKEN_SECRET is None:
        raise ValueError('Set TOKEN_SECRET before using this module')

    s = TimedJSONWebSignatureSerializer(TOKEN_SECRET, expires_in=expires)
    return s.dumps(kwargs)


def decode(token):
    """Decode a token

    Parameters
    ----------
    token: str
        The token to decode

    Returns
    -------
    dict
        Token content

    Raises
    ------
    Forbidden
        If token expired or invalid
    ValueError
        If TOKEN_SECRET is not set (use krieger_gae.set_token_secret)
    """
    TOKEN_SECRET = settings('TOKEN_SECRET')

    if TOKEN_SECRET is None:
        raise ValueError('Set TOKEN_SECRET before using this module')

    s = TimedJSONWebSignatureSerializer(TOKEN_SECRET)

    try:
        content = s.loads(token)
    except SignatureExpired:
        raise Forbidden('Expired token', u'El token es inválido')
    except BadSignature:
        raise Forbidden('Invalid token', u'El token es inválido')

    return content
