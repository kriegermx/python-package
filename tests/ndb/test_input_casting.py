import unittest
import datetime
from krieger_gae.ndb.input import cast_value_to_ndb_property
from google.appengine.ext import ndb


class Nested(ndb.Model):
    integer = ndb.IntegerProperty()
    date = ndb.DateProperty()


class Structure(ndb.Model):
    integer = ndb.IntegerProperty()
    date = ndb.DateProperty()
    repeated = ndb.IntegerProperty(repeated=True)
    nested = ndb.LocalStructuredProperty(Nested)


class CastingModel(ndb.Model):
    integer = ndb.IntegerProperty()
    float_ = ndb.FloatProperty()
    unicode_ = ndb.StringProperty()
    time = ndb.TimeProperty()
    date = ndb.DateProperty()
    dates = ndb.DateProperty(repeated=True)
    datetime = ndb.DateTimeProperty()
    structured = ndb.StructuredProperty(Structure)
    local_structured = ndb.LocalStructuredProperty(Structure)


class TestInputCasting(unittest.TestCase):

    def test_raises_error_if_unsupported_property(self):
        time_prop = CastingModel._properties['time']

        with self.assertRaises(NotImplementedError):
            cast_value_to_ndb_property('value', time_prop)

    def test_ignores_date_if_already_a_date_object(self):
        date_prop = CastingModel._properties['date']
        date = datetime.date.today()
        parsed_date = cast_value_to_ndb_property(date, date_prop)
        assert parsed_date == date

    def test_can_parse_date(self):
        date_prop = CastingModel._properties['date']
        parsed_date = cast_value_to_ndb_property(u'2010-01-01', date_prop)
        assert isinstance(parsed_date, datetime.date)

    def test_can_parse_nested_date(self):
        prop = CastingModel._properties['structured']
        d = dict(date=u'1999-01-01')
        parsed = cast_value_to_ndb_property(d, prop)
        assert isinstance(parsed, Structure)

    def test_can_parse_double_nested_date(self):
        prop = CastingModel._properties['structured']
        d = dict(nested=dict(date=u'1999-01-01'))
        parsed = cast_value_to_ndb_property(d, prop)
        assert isinstance(parsed, Structure)

    def test_raises_error_if_incorrect_date_format(self):
        date_prop = CastingModel._properties['date']

        with self.assertRaises(ValueError):
            cast_value_to_ndb_property(u'not-a-date', date_prop)

    def test_can_parse_datetime(self):
        datetime_prop = CastingModel._properties['datetime']
        parsed_date = cast_value_to_ndb_property(u'2010-01-01 01:01:01',
                                                 datetime_prop)
        assert isinstance(parsed_date, datetime.datetime)

    def test_raises_error_if_incorrect_datetime_format(self):
        datetime_prop = CastingModel._properties['datetime']

        with self.assertRaises(ValueError):
            cast_value_to_ndb_property(u'not-a-datetime', datetime_prop)

    def test_can_parse_structured_property(self):
        structured_property = CastingModel._properties['structured']
        d = dict(integer=1, repeated=[1, 2, 3])
        parsed_structured = cast_value_to_ndb_property(d,
                                                       structured_property)
        assert isinstance(parsed_structured, Structure)

    def test_can_parse_local_structured_property(self):
        structured_property = CastingModel._properties['local_structured']
        d = dict(integer=1, repeated=[1, 2, 3])
        parsed_structured = cast_value_to_ndb_property(d,
                                                       structured_property)
        assert isinstance(parsed_structured, Structure)

    def test_can_parse_nested_structured_property(self):
        structured_property = CastingModel._properties['structured']
        d = dict(integer=1, nested=dict(integer=1))
        parsed_structured = cast_value_to_ndb_property(d, structured_property)
        assert isinstance(parsed_structured, Structure)

    def test_can_parse_repeated_dates(self):
        date_prop = CastingModel._properties['dates']
        values = [u'2010-01-01', u'2010-01-01']
        parsed_dates = cast_value_to_ndb_property(values, date_prop)
        assert all([isinstance(p, datetime.date) for p in parsed_dates])

    def test_can_parse_integer(self):
        prop = CastingModel._properties['integer']
        parsed = cast_value_to_ndb_property(u'1', prop)
        assert isinstance(parsed, int)

    def test_can_parse_float(self):
        prop = CastingModel._properties['float_']
        parsed = cast_value_to_ndb_property(u'1.1', prop)
        assert isinstance(parsed, float)

    def test_can_parse_unicode(self):
        prop = CastingModel._properties['unicode_']
        parsed = cast_value_to_ndb_property(u'a string', prop)
        assert isinstance(parsed, unicode)
