from functools import reduce


def prettify_iterator(iterable):
    """Returns a prettified string representation of an iterable
    """
    return reduce(lambda x, y: x+', '+y, iterable)
