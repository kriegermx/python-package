from .models import Post, User
from .permissions import permissions

from krieger_gae.ndb.ObjectGuard import ObjectGuard
from krieger_gae.ndb.UserGuard import UserGuard


class MyUserGuard(UserGuard):
    """Subclass UserGuard to use your User class
    """
    MODEL = User


class PostGuard(ObjectGuard):
    """
    Subclass ObjectGuard to use your Post class you can also use
    ObjectGuard without subclassing, you can see how in the example
    """
    MODEL = Post
