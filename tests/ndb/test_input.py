import unittest

from krieger_gae.ndb.input import (_ensure_key_matches_kind, _convert_to_keys,
                                   _parse_and_validate_urlsafekeys_in,
                                   _map_parameters_in_fn_call,
                                   parse_and_validate_urlsafe_keys_in_args,
                                   validate_input_form,
                                   validate_namespace_in_keys)
from krieger_gae.exceptions import NotFound, BadRequest, Forbidden

from google.appengine.ext.ndb.key import Key
from google.appengine.ext import ndb


def dict_compare(d1, d2):
    """Compare two dictionaries

    Notes
    -----
    http://stackoverflow.com/questions/4527942/comparing-two-dictionaries-in-python
    """
    d1_keys = set(d1.keys())
    d2_keys = set(d2.keys())
    intersect_keys = d1_keys.intersection(d2_keys)
    same = set(o for o in intersect_keys if d1[o] == d2[o])
    return same == intersect_keys == d1_keys == d2_keys


class AnotherModelInput(ndb.Model):
    an_integer_prop = ndb.IntegerProperty()


class StructuredInput(ndb.Model):
    an_integer_prop = ndb.IntegerProperty()
    a_nested_structured_prop = ndb.StructuredProperty(AnotherModelInput)
    a_repeated_nested_structured_prop = ndb.StructuredProperty(AnotherModelInput,
                                                               repeated=True)


class ModelInput(ndb.Model):
    not_a_key = ndb.StringProperty()
    a_key = ndb.KeyProperty(kind=AnotherModelInput)
    a_required_prop = ndb.StringProperty(required=True)
    an_integer_prop = ndb.IntegerProperty()
    a_structured_prop = ndb.StructuredProperty(StructuredInput)
    a_repeated_prop = ndb.IntegerProperty(repeated=True)


class DummyGuard(object):
    def __init__(self, namespace=None):
        self._namespace = namespace

    @parse_and_validate_urlsafe_keys_in_args('a_key', ModelInput)
    def parse_and_validate(self, a_key):
        return a_key

    @parse_and_validate_urlsafe_keys_in_args('a_key', 'ModelInput')
    def parse_and_validate_with_classname(self, a_key):
        return a_key

    @parse_and_validate_urlsafe_keys_in_args('generic_key', None)
    def parse_and_validate_with_generic_key(self, generic_key):
        return generic_key

    @validate_namespace_in_keys
    def validate_namespace(self, param=None):
        pass


class TestInput(unittest.TestCase):
    def setUp(self):
        self.key = Key('Person', 1)

    #############################
    # Validating keys namespace #
    #############################

    def test_validation_rejects_if_wrong_namespace_in_simple_parameter(self):
        # Fake param.namespace()
        param = Key('Entity', 1, namespace='another_namespace')

        guard = DummyGuard(namespace='user_namespace')

        msg = "Cannot perform operation on object from a different namespace:"
        with self.assertRaisesRegexp(Forbidden, msg):
            guard.validate_namespace(param=param)

    def test_validation_rejects_if_wrong_namespace_in_repeated_parameter(self):
        param = Key('Entity', 1, namespace='another_namespace')

        guard = DummyGuard(namespace='user_namespace')

        msg = "Cannot perform operation on object from a different namespace:"
        with self.assertRaisesRegexp(Forbidden, msg):
            # 'hide' key in a list - the decorator should still be able
            # to get them
            guard.validate_namespace(param=[param])

    #############################

    def test_ensure_key_matches_kind_raises_error_if_unmatched_key(self):
        with self.assertRaises(NotFound):
            _ensure_key_matches_kind(self.key, 'NotAPerson')

    def test_ensure_to_key_from_urlsafe_works_with_urlsafe_keys(self):
        key = _convert_to_keys(self.key.urlsafe())
        assert key == self.key

    def test_ensure_to_key_from_urlsafe_works_with_keys(self):
        key = _convert_to_keys(self.key)
        assert key == self.key

    @unittest.expectedFailure
    def test_args_and_kwargs_parsing(self):
        def a_function(a, b, c):
            return None

        args = [1]
        kwargs = dict(b=2, c=3)
        parsed = _map_parameters_in_fn_call(args, kwargs, a_function)
        assert parsed['a'] == 1 and parsed['b'] == 2 and parsed['c'] == 3

    @unittest.expectedFailure
    def test_parsing_and_validating_keys_fails_if_not_a_key(self):
        def do_someting_with_a_model(a_key):
            pass

        args = ['this is not a key']
        kwargs = {}

        with self.assertRaises(NotFound):
            _parse_and_validate_urlsafekeys_in(ModelInput, args, kwargs,
                                               do_someting_with_a_model,
                                               'a_key')

    #####################################################
    # Parsing and validating keys in function arguments #
    #####################################################

    def test_can_parse_and_validate_ignores_keys_in_kwargs(self):
        a_key = Key('ModelInput', 1)
        guard = DummyGuard()
        result = guard.parse_and_validate(a_key=a_key)

        assert result == a_key

    def test_can_parse_and_validate_parses_urlsafekeys_in_kwargs(self):
        a_key = Key('ModelInput', 1)
        guard = DummyGuard()
        result = guard.parse_and_validate(a_key=a_key.urlsafe())
        assert result == a_key

    def test_can_parse_and_validate_ignores_keys_in_args(self):
        a_key = Key('ModelInput', 1)
        guard = DummyGuard()
        result = guard.parse_and_validate(a_key)
        assert result == a_key

    def test_can_parse_and_validate_parses_urlsafekeys_in_args(self):
        a_key = Key('ModelInput', 1)
        guard = DummyGuard()
        result = guard.parse_and_validate(a_key.urlsafe())
        assert result == a_key

    def test_parsing_and_validating_raises_error_if_wrong_kwarg(self):
        a_key = Key('ModelInput', 1)
        guard = DummyGuard()
        with self.assertRaises(TypeError):
            guard.parse_and_validate(not_a_param=a_key, a_key=a_key)

    def test_parsing_skips_validation_if_None(self):
        a_key = Key('SomeModelThatDoesNotExist', 1)
        guard = DummyGuard()
        res = guard.parse_and_validate_with_generic_key(a_key.urlsafe())
        assert a_key == res

    def test_can_parse_and_validate_declaring_classname(self):
        a_key = Key('ModelInput', 1)
        guard = DummyGuard()
        result = guard.parse_and_validate_with_classname(a_key.urlsafe())
        assert result == a_key
