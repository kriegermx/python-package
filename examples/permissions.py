from .models import Post, User

from bouncer import authorization_method
from bouncer.constants import CREATE, READ, UPDATE, DELETE


@authorization_method
def permissions(user, they):
    """
    Define your bouncer permissions, apart from the bouncer.constants
    permissions you need to also define a LIST permission
    """
    they.can(('LIST', CREATE, READ, UPDATE, DELETE), Post)
    they.can(('LIST', CREATE, READ, UPDATE, DELETE), User)
