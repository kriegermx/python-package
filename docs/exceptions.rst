Exceptions
==========

These are the exceptions raised in the module, you can use them to catch them
in try-catch statements to reuse them in your app. All exceptions are Google
Cloud Endpoints compatible so if any of them is raised an appropriate response
will be sent. If you are using Flask you can use an error handler to catch
all or some of these exceptions to render an appropriate error template. A
meaningful error message is included whenever any of these exceptions is
raised.

.. automodule:: krieger_gae.exceptions
    :members: