import unittest

from krieger_gae.ndb.input import validate_input_form
from krieger_gae.exceptions import BadRequest

from google.appengine.ext import ndb
from .util import dict_compare


class AnotherModel(ndb.Model):
    integer = ndb.IntegerProperty()


class Structured(ndb.Model):
    integer = ndb.IntegerProperty()
    nested = ndb.StructuredProperty(AnotherModel)
    nested_repeated = ndb.StructuredProperty(AnotherModel, repeated=True)


class FormModel(ndb.Model):
    string = ndb.StringProperty()
    key = ndb.KeyProperty(kind=AnotherModel)
    required = ndb.StringProperty(required=True)
    integer = ndb.IntegerProperty()
    structured = ndb.StructuredProperty(Structured)
    repeated = ndb.IntegerProperty(repeated=True)


class TestInputParsing(unittest.TestCase):
    """Test functions for input parsing
    """

    def test_validate_input_form_ignores_empty_strings_and_none(self):
        data = dict(string=None, key='', required='a value')
        res = validate_input_form(data, FormModel)
        assert dict_compare(res, dict(required='a value'))

    def test_validate_input_form_rejects_if_wrong_keys_in_data(self):
        data = dict(not_a_prop='stuff', required='a value')
        with self.assertRaises(BadRequest):
            validate_input_form(data, FormModel)

    def test_validate_input_form_rejects_if_missing_required_properties(self):
        data = dict(string='stuff')
        with self.assertRaises(BadRequest):
            validate_input_form(data, FormModel)

    def test_validate_input_form_accepts_if_missing_req_and_not_check(self):
        data = dict(string='stuff')
        res = validate_input_form(data, FormModel, check_required=False)
        assert dict_compare(res, data)

    def test_validate_input_form_casts_properties_when_possible(self):
        data = dict(integer=u'10')
        res = validate_input_form(data, FormModel, check_required=False)
        assert dict_compare(res, dict(integer=10))

    def test_validate_input_form_validates_structured_properties(self):
        data = dict(structured=dict(not_a_prop='10'))
        with self.assertRaises(BadRequest):
            validate_input_form(data, FormModel, check_required=False)

    def test_validate_input_form_validates_nested_structured_properties(self):
        d = dict(not_a_prop=1)
        data = dict(structured=dict(nested=d))
        with self.assertRaises(BadRequest):
            validate_input_form(data, FormModel, check_required=False)

    def test_validate_input_form_casts_properties_in_structured_props(self):
        data = dict(structured=dict(integer=u'10'))
        res = validate_input_form(data, FormModel, check_required=False)
        assert isinstance(res['structured'], Structured)

    def test_validate_input_form_casts_properties_in_nested_structured_props(self):
        data = dict(structured=dict(nested=dict(integer=u'10')))
        res = validate_input_form(data, FormModel, check_required=False)
        assert isinstance(res['structured'], Structured)

    def test_validate_input_form_casts_repeated_properties(self):
        data = dict(repeated=[u'1', u'2'])
        res = validate_input_form(data, FormModel, check_required=False)
        assert dict_compare(res, dict(repeated=[1, 2]))

    def test_shows_error_if_missing_properties(self):
        data = dict(not_a_property='hehe')

        with self.assertRaisesRegexp(BadRequest, 'not_a_property'):
            validate_input_form(data, FormModel)

    def test_validation_ignores_unsupported_properties(self):
        data = dict(key=ndb.Key('FormModel', 1), required='a value')
        res = validate_input_form(data, FormModel)
        assert dict_compare(res, data)
