import json
import logging
from datetime import datetime
from google.appengine.ext.blobstore import blobstore

from ..exceptions import BadRequest

"""
Low-level module for parsing user input, mostly used in the input module
"""

logger = logging.getLogger(__name__)


def json_loads(json_str):
    """
    Wrapper for json.loads that raises a custom exception that sends
    an appropriate message through flask or endpoints
    """
    try:
        data = json.loads(json_str)
    except ValueError as e:
        raise BadRequest(message=e,
                         user_message='Error en el formato de los datos')
    return data


def to_datetime(s):
    """
    Parse a str and convert it to a datetime
    """
    return datetime.strptime(s, "%Y-%m-%d %H:%M:%S")


def to_date(s):
    """
    Parse a str and convert it to a date, if the object is
    already date, just return the value
    """
    return datetime.strptime(s, "%Y-%m-%d").date()


def to_blobkey(s):
    return blobstore.BlobKey(s)
