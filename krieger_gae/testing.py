import unittest

from google.appengine.ext import ndb
from google.appengine.ext import testbed

"""
Testing helper classes
"""


class SingleDatastoreTestCase(unittest.TestCase):
    """
    Subclass of `unittest.TestCase` which configures App Engine datastore
    for testing. Uses the same datastore copy for the entire class
    """

    @classmethod
    def setUpClass(cls):
        super(SingleDatastoreTestCase, cls).setUpClass()

        # First, create an instance of the Testbed class.
        cls.testbed = testbed.Testbed()
        # Then activate the testbed, which prepares the service stubs for use.
        cls.testbed.activate()
        # Next, declare which service stubs you want to use.
        cls.testbed.init_datastore_v3_stub()
        cls.testbed.init_memcache_stub()
        cls.testbed.init_blobstore_stub()
        cls.testbed.init_mail_stub()
        cls.mail_stub = cls.testbed.get_stub(testbed.MAIL_SERVICE_NAME)

        # root_path must be set the the location of queue.yaml.
        # Otherwise, only the 'default' queue will be available.
        cls.testbed.init_taskqueue_stub()
        cls.taskqueue_stub = cls.testbed.get_stub(testbed.TASKQUEUE_SERVICE_NAME)

        # Clear ndb's in-context cache between tests.
        # This prevents data from leaking between tests.
        # Alternatively, you could disable caching by
        # using ndb.get_context().set_cache_policy(False)
        ndb.get_context().clear_cache()

    @classmethod
    def tearDownClass(cls):
        super(SingleDatastoreTestCase, cls).tearDownClass()
        cls.testbed.deactivate()


class MultiDatastoreTestCase(unittest.TestCase):
    """
    Subclass of `unittest.TestCase` which configures App Engine datastore
    for testing. Restarts datastore in every test.
    """

    def setUp(self):
        super(MultiDatastoreTestCase, self).setUp()

        # First, create an instance of the Testbed class.
        self.testbed = testbed.Testbed()
        # Then activate the testbed, which prepares the service stubs for use.
        self.testbed.activate()

        # Next, declare which service stubs you want to use.
        self.testbed.init_datastore_v3_stub()
        self.testbed.init_memcache_stub()
        self.testbed.init_blobstore_stub()
        self.testbed.init_mail_stub()
        self.mail_stub = self.testbed.get_stub(testbed.MAIL_SERVICE_NAME)

        # root_path must be set the the location of queue.yaml.
        # Otherwise, only the 'default' queue will be available.
        self.testbed.init_taskqueue_stub()
        self.taskqueue_stub = self.testbed.get_stub(testbed.TASKQUEUE_SERVICE_NAME)

        # Clear ndb's in-context cache between tests.
        # This prevents data from leaking between tests.
        # Alternatively, you could disable caching by
        # using ndb.get_context().set_cache_policy(False)
        ndb.get_context().clear_cache()

    def tearDown(self):
        super(MultiDatastoreTestCase, self).tearDown()
        self.testbed.deactivate()
