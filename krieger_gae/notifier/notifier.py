# -*- coding: utf-8 -*-
import json
import logging
import os

from google.appengine.api import mail as google_mail
from google.appengine.ext import deferred
from google.appengine.ext.ndb.key import Key
from jinja2 import Environment, select_autoescape, Template
import sendgrid
from sendgrid.helpers import mail as sengrid_mail


class Notifier(object):
    """Notification manager

    This class provides an abstraction for sending notifications to your users,
    it handles notification rendering from jinja templates and email
    notifications.

    You need to provide a ndb.Model that will be used as the Notification
    model (every notification gets stored), the model must contain at minimum
    the following properties: title (str), content (str),
    resource (a ndb Key pointing to the object related with the notification),
    extra (a json property for storing extra info)  and assigned (a
    KeyProperty pointing to the user recipient).

    The current implementation can use SendGrid (https://sendgrid.com/)
    to handle large volumes of mail. In order to use this platform set
    the environment variable USE_SENDGRID=True and the variable SENDGRID_API_KEY=your_sendgrid_api_key in the App Engine configuration of your ´project.

    Notifications are stored in the same namespace as the User key.

    Parameters
    ----------
    notification_class: ndb.Model
        Notification class
    email_master: str
        Email sender
    path_to_loader: jinja2 loader, optional
        jinja's loader, see jinja2 docs for more
    template_loader: callable, optional
        Optional function to be used before rendering title and content, if
        you are sending placeholders use this to pass a function to load
        the templates (e.g. if you are providing multi
        language notifications)
    save_rendered: bool, optional
        Whether to save rendered data or just the templates, if you decide
        to save the templates you need to manually provide a rendering
        method for the notifications

    Examples
    --------

    .. literalinclude:: ../examples/notifier.py

    """

    def __init__(self, notification_class=None, email_master=None,
                 loader=None, template_loader=None,
                 save_rendered=True):
        self.template_loader = (template_loader if template_loader
                                else lambda x: x)

        self.save_rendered = save_rendered
        self.notification_class = notification_class
        self.email_master = email_master

        # Create jinja environment
        autoescape = select_autoescape(['html', 'xml'])
        self.env = Environment(loader=loader, autoescape=autoescape)

        self.logger = logging.getLogger(__name__)

    def send(self, recipient, title, content, email_template=None,
                render_email_in_content_tag=True, push=False, send_now=True,
                resource=None, extra=None, extra_email=None,
                extra_notification=None):
        """
        A notification is just a record in the datastore, options are available
        to send a push notification and to send emails.

        Send a notification to a certain user. The only required parameters
        are the user Key (or urlsafe key) and the title.

        Parameters
        ----------
        recipient: Key or urlsafe Key
            User that will receive the notification
        title: str
            Notification's title, can be a template rendered using the template
            loader
        content: str
            Notification's content, can be a template rendered using the
            template loader
        email_template: str, optional
            Template name to use for the email, it does not send email if None
        render_email_in_content_tag: bool
            If True, it renders the email template by first rendering the
            content with extra and then passing the result to the email
            template with a content tag, if False it renders the email
            template directly passing extra
        push: bool
            Sends push notification with rendered content and title
        send_now: bool
            If push or email are True this decides if the notification will be
            send now or put into the task queue
        extra_email: dict, optional
            Optional parameters to be passed to the email template, this is
            useful when you want to include extra information in the email
            that is NOT needed in the notification object, such as passing
            a URL. This won't be stored in the Notification object
        extra_notification: dict, optional
            Extra parameters passed to the Notification model constructor,
            defaults to None

        Notes
        -----
        The recipient key is used to retrieve the user object, by default
        it is assumed that user.name and user.email retrieve the user's name
        and user's email respectively but you can customize this behavior
        by creating a subclass and replacing get_name and get_email
        """
        # try to parse recipient in case is a urlsafe key
        try:
            recipient = Key(urlsafe=recipient)
        except Exception:
            pass

        extra = extra if extra else dict()
        extra_email = extra_email if extra_email else dict()
        extra_notification = (extra_notification if extra_notification
                              else dict())

        title = (Template(self.template_loader(title)).render(**extra)
                 if self.save_rendered else title)

        try:
            content = (Template(self.template_loader(content)).render(**extra)
                       if self.save_rendered else content)
        except Exception as e:
            raise Exception('Error ({}) rendering content {} with extra {}'
                            .format(e, content, json.dumps(extra, indent=4)))

        notification_extra = None if self.save_rendered else extra

        # only save extra when saving
        # templates
        # TODO: optionall save extra
        # even if saving rendered
        # version
        data = dict(title=title,
                    content=content,
                    resource=None if not resource else Key(urlsafe=resource),
                    extra=notification_extra,
                    assigned=recipient,
                    namespace=recipient.namespace())

        data.update(extra_notification)

        notification = self.notification_class(**data)
        notification.put()

        self.logger.debug('Notification stored: {}'.format(notification))

        if email_template is not None:
            template_content = self.env.get_template(email_template)

            if render_email_in_content_tag:
                rendered = (Template(self.template_loader(content))
                            .render(**extra))
                html_version = template_content.render(content=rendered,
                                                       **extra_email)
            else:
                extra.update(extra_email)
                html_version = template_content.render(**extra)

            self._send_email_from_notification(notification, html_version,
                                               send_now)


    def send_newsletter(self, recipients, subject, html_version):
        """Send a newsletter, emails are put into a task queue

        Parameters
        ----------
        recippients: list
            Lisf of user keys
        subject: str
            Email subject
        html_version: str
            Email content
        """

        def _make_message(recipient, subject, html_version):
            message = google_mail.EmailMessage(sender=self.email_master)
            email = self.get_email(recipient)
            name = self.get_name(recipient)

            message.to = "{} <{}>".format(name, email)
            message.html = html_version
            message.body = html_version
            message.subject = subject

            return message

        messages = [_make_message(r, subject, html_version) for r
                    in recipients]

        for m in messages:
            deferred.defer(_send_email, m, self.use_sendgrid)

    def _send_email_from_notification(self, notification, html_version,
                                      send_now):
        """Send email from a notification object
        """
        message = google_mail.EmailMessage(sender=self.email_master)
        user = notification.assigned.get()
        email = self.get_email(user)
        name = self.get_name(user)

        message.to = "{} <{}>".format(name, email)
        message.html = html_version
        message.body = html_version
        message.subject = notification.to_dict()['title']

        if send_now:
            _send_email(message)
        else:
            deferred.defer(_send_email, message)

    def get_email(self, user):
        """Get the email from the user object
        """
        return user.email

    def get_name(self, user):
        """Get the name from the user object
        """
        return user.name


def _send_email(message):
    """Wrapper for sending emails through deferred library
    """
    logger = logging.getLogger(__name__)
    logger.debug('\n-----------------------------'
                 '\nSending email to:{to}'
                 '\nSubject: {subject}'
                 '\nBody:\n{content}'
                 '\n-----------------------------\n'
                 .format(subject=message.subject,
                         to=message.to,
                         content=message.html))
    use_sendgrid = bool(os.getenv('USE_SENDGRID', False))
    if use_sendgrid:
        _send_sendgrid_mail(message)
    else:
        _send_google_mail(message)


def _send_google_mail(message):
    message.send()
    return None


def _send_sendgrid_mail(message):
    sendgrid_api_key = os.getenv('SENDGRID_API_KEY')
    if not sendgrid_api_key:
        raise Exception("You must specify your SendGrid API key in the environment variable SENDGRID_API_KEY")
    sg = sendgrid.SendGridAPIClient(apikey=sendgrid_api_key)
    to_email = sengrid_mail.Email(message.to)
    from_email = sengrid_mail.Email(message.sender)
    content = sengrid_mail.Content('text/html', message.html)
    message = sengrid_mail.Mail(from_email, message.subject, to_email, content)
    response = sg.client.mail.send.post(request_body=message.get())
    return response
