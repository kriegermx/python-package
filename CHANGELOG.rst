Changelog
=========

v0.10dev - November 13, 2018 - juanjo@krieger.mx
-------
* Using SendGrid to handle high-volume mail
* Adds deferred notification sending using deferred library

v0.9dev
-------

* Updates notifier to use an assigned property in notification objects instead of setting the user as the parent key, the former enables greater query flexibility
* list_with_request_args now accepts **kwargs to be passed to the list function
* adds instrospector class_for_structured_property method to get class for structured properties

v0.8dev - July 5, 2017
----------------------

* Adds new option to notifier to include extra tags for email rendering
* list_with_request_args now supports ordering by a single value
* Notifier.send.recipient now accepts both urlsafe keys and keys
* Fixes bug in UserGuard.set_password_with_restore_token, password encryption was being done twice
* Fixes a bug in exceptions that was causing the to_dict method not to include user_message
* Updates notifier to create notifications with an assigned property pointing to the user

v0.7 - May 19, 2017
-------------------

* Adds Notifier
* Adds command line tool to generate constants declarations for python and js
* Fixes bug in ObjectGuard.validate, hardcoded check_required value
* Fixes bug @ input._map_parameters_in_fn_call that was causing missing args to be set to None
* Improved logging when failing to parse parameteres @ parse_url_params
* Changes some test ndb model names to avoid name clashing (this was breaking some tests in bitbucket pipelines)

v0.6 - April 9, 2017
--------------------

* Improved error message when trying to load a user and datastore returns None
* Added support for ImmutableMultiDict in list_with_request_args
* Adds token section in documentation
* input._ensure_key_matches_kind checks keys againts root class when comparing polymodel keys
* Refactors input parsing (cast_value_to_ndb_property function)

v0.5 - March 23, 2017
---------------------

* Refactors input module: cleaner code and removes unused functions taken from the ossa codebase
* Moves some functions from input to a new parsers module
* Bug fixes in input.list_with_request_args, adds docs for the function

v0.4 - March 16, 2017
---------------------

* Improved documentation in UserGuard regarding exceptions raised
* Makes token a top-level module
* UserGuard: Improved functions for account activation
* UserGuard: Adds functions for password storage
* Fixes bug in UserGuard due to creating instances using UserGuard instead of cls in some methods


v0.3 - March 9, 2017
--------------------

* Improved documentation for email validator
* Adds api module
* Improves documentation structure
* Improves package structure

v0.2 - March 8, 2017
--------------------

* `TOKEN_SECRET` can now be set using `krieger_gae.set_token_secret`
* Enhanced compatibility with flask-login
* Improved namespace management

v0.1 - March 8, 2017
--------------------

* Adds basic modules from OSSA codebase
* Adds basic documentation
* Adds basic test suite
