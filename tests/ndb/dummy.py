from krieger_gae.ndb import ObjectGuard
from krieger_gae.ndb import UserGuard

from google.appengine.ext import ndb
from bouncer import authorization_method
from bouncer.constants import CREATE, READ, UPDATE, DELETE


class User(ndb.Model):
    name = ndb.StringProperty()
    password = ndb.StringProperty()
    email = ndb.StringProperty(required=True)
    role = ndb.StringProperty()
    status = ndb.StringProperty()

    def list_query(self, cls, namespace):
        # TODO: use namespace?
        return cls.query()


@authorization_method
def permissions(user, they):
    they.can(('LIST', CREATE, READ, UPDATE, DELETE), Model)
    they.can(('LIST', CREATE, READ, UPDATE, DELETE), User)


class Model(ndb.Model):
    rep1 = ndb.IntegerProperty(repeated=True)
    prop1 = ndb.IntegerProperty(repeated=False)

    def to_dict(self):
        d = super(Model, self).to_dict()
        d['key'] = self.key.urlsafe()
        return d


class ModelWithRequired(ndb.Model):
    prop = ndb.IntegerProperty(required=True)


class Guard(ObjectGuard):
    MODEL = Model


class MyUserGuard(UserGuard):
    MODEL = User
