import unittest
from krieger_gae.ndb.mixins import IsDirtyMixin
from google.appengine.ext import ndb


class DummyModel(IsDirtyMixin, ndb.Model):
    name = ndb.StringProperty()
    email = ndb.StringProperty()


class DummyModel2(IsDirtyMixin, ndb.Model):
    number = ndb.IntegerProperty()

    def _post_put_hook(self, future):
        raise Exception


class IsDirtyMixinTesting(unittest.TestCase):
    def test_marks_attribtues_as_dirty_when_changed(self):
        user = DummyModel(name='name')
        user.name = 'new name'
        assert user.is_dirty('name')

    def test_returns_set_of_dirty_properties(self):
        user = DummyModel(name='a name', email='myemail@stuff.com')
        user.name = 'new name'
        user.email = 'newemail@stuff.com'
        assert user.dirty_properties() == {'name', 'email'}

    def test__post_put_hook_still_gets_called(self):
        with self.assertRaises(Exception):
            DummyModel2(number=1).put()
