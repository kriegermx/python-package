import unittest
from google.appengine.ext import ndb

from krieger_gae.ndb.introspection import DatastoreModelIntrospector


class IntrospectorPhone(ndb.Model):
    number = ndb.StringProperty()


class IntrospectorAddress(ndb.Model):
    street = ndb.StringProperty()
    number = ndb.StringProperty()


class IntrospectorModel(ndb.Model):
    name = ndb.StringProperty()
    email = ndb.StringProperty()
    phone = ndb.StructuredProperty(IntrospectorPhone, repeated=True)
    address = ndb.StructuredProperty(IntrospectorAddress)


class IntrospectorTesting(unittest.TestCase):
    def test_can_get_class_for_structured_property(self):
        insp = DatastoreModelIntrospector(IntrospectorModel)
        klass = insp.class_for_structured_property('address')
        klass == IntrospectorAddress

    def test_can_get_class_for_repeated_structured_property(self):
        insp = DatastoreModelIntrospector(IntrospectorModel)
        klass = insp.class_for_structured_property('phone')
        klass == IntrospectorPhone
