# -*- coding: utf-8 -*-
from ..exceptions import Forbidden

from google.appengine.ext.ndb.key import Key

import collections
import logging
import inspect
from copy import copy

from functools import wraps
from bouncer import ensure as bouncer_ensure
from bouncer.exceptions import AccessDenied

logger = logging.getLogger(__name__)


def ensure(*args, **kwargs):
    """Wrapper for bouncer.ensure so the appropriate exception is raised
    """
    try:
        bouncer_ensure(*args, **kwargs)
    except AccessDenied:
        raise Forbidden(message="The user does not have permission to perform"
                                " the operation",
                        user_message="No tienes permisos para realizar la "
                                     "acción solicitiada")


def _map_parameters_in_fn_call(args, kwargs, func):
    """
    Based on function signature, parse args to to convert them to key-value
    pairs and merge them with kwargs

    Any parameter found in args that does not match the function signature
    is still passed.
    """
    # Get missing parameters in kwargs to look for them in args
    args_spec = inspect.getargspec(func).args
    params_all = set(args_spec)
    params_missing = params_all - set(kwargs.keys())

    # Remove self parameter from params missing since it's not used
    if 'self' in params_missing:
        params_missing.remove('self')
    else:
        raise ValueError(("self was not found in the fn signature, "
                          " this is intended to be used with instance "
                          "methods only"))

    # Get indexes for those args
    # sum one to the index to account for the self parameter
    idxs = [args_spec.index(name) for name in params_missing]

    # Parse args
    args_parsed = dict()
    for idx in idxs:
        key = args_spec[idx]

        try:
            value = args[idx-1]
        except IndexError:
            pass
        else:
            args_parsed[key] = value

    parsed = copy(kwargs)
    parsed.update(args_parsed)

    return parsed


def vectorize_parameter(name):
    def vectorize(func):

        @wraps(func)
        def func_wrapper(self, *args, **kwargs):
            params = _map_parameters_in_fn_call(args, kwargs, func)
            value = params.pop(name)
            print(params, value)
            if _is_collection(value):
                return [func(o, **params) for o in value]
            else:
                return func(value, **params)

        return func_wrapper

    return vectorize


def _unwrap_mixed_iterator(mixed_iterator):
    """ [[1,2,3], 4 [5,6]] -> [1,2,3,4,5,6]
    """
    unwrapped = []
    for element in mixed_iterator:
        if _is_collection(element):
            unwrapped.extend(element)
        else:
            unwrapped.append(element)
    return unwrapped


def _is_collection(obj):
    """Determine wheter obj is an interable (excluding strings) or not
    """
    iterable = isinstance(obj, collections.Iterable)
    string = isinstance(obj, basestring)
    return iterable and not string


def _wrap_in_list(obj):
    if _is_collection(obj):
        return obj
    else:
        return [obj]


def _key2urlsafe(key):
    """
    Converts a Key or iterable of Keys to their corresponding urlsafe versions
    """
    if _is_collection(key):
        return [k.urlsafe() for k in key]
    else:
        return key.urlsafe()


def _urlsafe2key(key):
    """
    Converts a Key or iterable of Keys to their corresponding urlsafe versions
    """
    if _is_collection(key):
        return [Key(urlsafe=k) for k in key]
    else:
        return Key(urlsafe=key)


def _safe_to_dict(prop):
    d = prop.get().to_dict() if prop else None
    return d


def _safe_condensed(prop):
    # First check ir model.property returns a Key
    if prop:

        # If it returns a key try to get the object
        value = prop.get()

        # If it returns a value, get condensed
        if value:
            try:
                return prop.get().to_condensed_dict()
            except AttributeError:
                # If no condensed just call dict
                return prop.get().to_dict()

        # Key returned empty, value this probably means that the object
        # the key is refering to was deleted
        else:
            logger.info("KeyProperty {} returned None".format(prop))
            return None
    else:
        return None


def _safe_urlsafe_blob_key(key):
    if not key:
        return None
    elif _is_collection(key):
        return [k.ToXml() for k in key]
    else:
        return key.ToXml()


def _model_has_property(cls, property):
    try:
        getattr(cls, property)
    except AttributeError:
        return False
    else:
        return True


def _is_key_or_iterable_of_keys(value):
    """
    Takes an object (or list of objects) and checks if every item is a Key
    in case of a list it checks if every element is a Key
    """
    if _is_collection(value):
        key = [isinstance(v, Key) for v in value]
        return all(key)
    elif isinstance(value, Key):
        return True
    else:
        False
