Datastore
=========

The `ndb` module provides to classes for abstracting the use of permissions
and validating user input. `ObjectGuard` is intended to be used for any
datastore you define and `UserGuard` is intended to be used for your User
model as it provides several methods to ease account creation and login.

There are some other useful modules `validators` to validate ndb.Model
property format, `mixins` for ndb.Model.

Some examples are also provided.

.. toctree::
   :maxdepth: 2

   ndb/objectguard
   ndb/userguard
   ndb/validators
   ndb/mixins
   ndb/input
   ndb/examples