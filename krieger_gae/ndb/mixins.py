from collections import defaultdict


class IsDirtyMixin(object):
    """
    Extends a NDB model class to provide a method that checks if a property
    has been modified and not saved. This is useful when permissions are
    applied to specific properties in an ndb model

    Examples
    --------

    .. literalinclude:: ../../examples/isdirty_mixin.py

    Notes
    -----
    *Important:* does not work with repeated properties

    """
    # Override init method to add a dictionary to keep track of dirty
    # properties
    def __init__(self, *args, **kwargs):
        super(IsDirtyMixin, self).__init__(*args, **kwargs)
        self.__dirty = defaultdict(lambda: False)

    def __setattr__(self, name, value):
        super(IsDirtyMixin, self).__setattr__(name, value)
        if not name.startswith('_'):
            self.__dirty[name] = True

    # Set flags to false when saving to the database
    def _post_put_hook(self, future):
        self.__dirty = defaultdict(lambda: False)

    # To check if a property has been modified and still not saved to the db
    def is_dirty(self, name):
        return self.__dirty[name]

    # Returns a set with all dirty properties
    def dirty_properties(self):
        return {k for k, v in self.__dirty.items() if v}
