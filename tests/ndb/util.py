def dict_compare(d1, d2):
    """Compare two dictionaries

    Notes
    -----
    http://stackoverflow.com/questions/4527942/comparing-two-dictionaries-in-python
    """
    d1_keys = set(d1.keys())
    d2_keys = set(d2.keys())
    intersect_keys = d1_keys.intersection(d2_keys)
    same = set(o for o in intersect_keys if d1[o] == d2[o])
    return same == intersect_keys == d1_keys == d2_keys
