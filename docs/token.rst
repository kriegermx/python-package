Tokens
======

This module provides functions for encoding and decoding as well as the token
definition used by :class:`UserGuard <krieger_gae.ndb.UserGuard>`.

.. automodule:: krieger_gae.token
    :members: