# -*- coding: utf-8 -*-
import logging
from copy import copy

# from ..notifications.center import NotificationCenter
from ..token import generate, decode, TokenType

from .FrozenJSON import FrozenJSON
from ..exceptions import (BadRequest, NotFound, InternalServerError,
                          Unauthorized)
from .helpers import _wrap_in_list
from .input import (parse_and_validate_urlsafe_keys_in_args,
                    validate_input_form, validate_namespace_in_keys,
                    parse_urlsafe_keys_in_kwargs_with_model,
                    parse_and_validate_urlsafe_keys_in_args_with_model)
from .constants import UserStatus

from google.appengine.datastore.datastore_query import Cursor
from google.appengine.ext.ndb.key import Key
from bouncer import can
from .helpers import ensure
from bouncer.constants import CREATE, READ, DELETE, UPDATE
from passlib.hash import sha256_crypt

logger = logging.getLogger(__name__)

MIN_PASSWORD_LENGTH = 6


class UserGuard(object):
    """
    This class takes care of operations on users. flask-login can be configured
    that this class isn't directly used, instead
    when a user is logged into the platform, the variable current_user
    will hold an instance of this class with the curently logged in
    user.

    Parameters
    ----------
    user: User
        User instance used for permissions
    notifier: Object
        An object that will be called when certain actions happen.
        Defaults to NOTIFIER class variable

    Notes
    -----
    This class makes use of tokens for some account operations, each token
    has a type to prevent certain token to be used for another opertaion
    (e.g. a restore password cannot be used for creating a new account),
    the  values used are described in
    :class:`TokenType <krieger_gae.token.TokenType>` if you used the Token
    module for custom tokens you can define your own TokenTypes as long as you
    keep the ones in TokenType with the same values.

    Examples
    --------
    Userguard that uses the User model

    .. code-block:: python

        class MyUserGuard(UserGuard):
            MODEL = User

    """
    MODEL = None
    NOTIFIER = None

    def __init__(self, user, notifier=None):
        self._user = user
        self._model = self.MODEL
        self._namespace = self._user.key.namespace()
        self._notifier = notifier or self.NOTIFIER
        self.logger = logging.getLogger(__name__)

    ############################
    # flask-login requirements #
    ############################

    @property
    def is_authenticated(self):
        return True

    @property
    def is_active(self):
        return self._user.status == UserStatus.Active

    @property
    def is_anonymous(self):
        return False

    def get_id(self):
        """Returns user id (email)
        """
        return self._user.email

    ###################################
    # end of flask-login requirements #
    ###################################

    @parse_urlsafe_keys_in_kwargs_with_model
    def create_account(self, **kwargs):
        """
        Create an account for a new user. If the user does not have a Person
        associated, it returns a token than can be used to set a password,
        otherwise it just creates a User.

        Parameters
        ----------
        **kwargs
            The arguments sent to the Person constructor, these parameters
            should be any valid Person property. See the Person model to check
            the properties names and types.

        Returns
        -------
        token: str
            A token that can be used to create a new user account, only
            returned if the user does not have a Person instance alrady
            otherwise returns None
        user: FrozenJSON
            A FrozenJSON instance built from the new user (whose account
            is inactive until a password is generated)

        Raises
        ------
        Forbidden
            If the user does not have permission to create a user with the
            given information
        BadRequest
            If something went wrong with tne input provided (e.g. there's a
            user with the email provided)

        Notes
        -----
        The names and types in kwargs must match the corresponding User model,
        the only exception are KeyProperties, for these type of properties you
        can pass either a Key or just the ID for the object and the object
        will internally build the Key from the ID.

        The email addresses is converted to lowercase before saving to the
        datatore
        """
        user = self._validate_account_data(kwargs)
        user.put()

        frozen_user = FrozenJSON(user.to_dict())

        # Need to create a new account, send token
        token = generate(user_key=user.key.urlsafe(),
                         origin_key=self.key,
                         type=TokenType.SignUp)

        # Let the notifier know that the operation was successful
        if self._notifier:
            data = copy(kwargs)
            data['origin'] = self.key
            data['namespace'] = self._namespace
            data['token'] = token
            data['user'] = frozen_user
            self._notifier.create_account(data=data)

        return token, frozen_user

    def _validate_account_data(self, kwargs):
        """
        Validate and check permissions before creating an account

        Parameters
        ----------

        Returns
        -------
        Person
            A (unsaved) Person instance
        bool
            A flag indicating whether the person needs a token to create the
            account

        Raises
        ------
        Forbidden
            If the user does not have permission to create the account
        BadRequest
            If data to create the user is invalid
        """
        # Check that there are no private properties in kwargs
        private_properties = set(('password', 'status'))
        passed_keys = set(kwargs.keys())
        passed_private_props = passed_keys.intersection(private_properties)

        if len(passed_private_props):
            message = ('The following values are not allowed: {}'
                       .format(passed_private_props))
            user_message = u'Hubo un error al validar los datos de entrada'
            raise BadRequest(message, user_message)

        kwargs = validate_input_form(kwargs, self.MODEL, check_required=True)

        # Convert e-mail to lowercase
        kwargs['email'] = kwargs['email'].lower()
        kwargs['status'] = UserStatus.Pending

        other = self.MODEL(**kwargs)

        # Validate permissions
        ensure(self._user, CREATE, other)

        user = self.MODEL.query(self.MODEL.email == kwargs['email'],
                                namespace=self._namespace).get()
        if user:
            message = ('An user with {} email already exists'
                       .format(kwargs['email']))
            user_message = (u'Ya existe un usuario con el correo seleccionado'
                            .format(kwargs['email'])) 
            raise BadRequest(message=message, user_message=user_message)

        return other

    def list(self, count=10, offset=0, cursor=None, filter=None, order=None,
             get_cursor=False):
        """
        Get a list of users, the list provided takes into account the
        permissions for the given user so this method will list every user
        the current user has permission to see.

        Parameters
        ----------
        count: int
            Maximum number of elements to return
        offset: int
            Number of elements to skip
        cursor: str
            urlsafe cursor to continue querying
        get_cursor: boolean
            if this is True, the first time you call the function the valuesm
            the cursor and a more flag will be returned. Ignores offset
            if True.
        filter: Filter
            Filter to apply, must be a valid Filter NDB expression
        order: Order
            How to sort the results, must be a valid Order NDB expression

        Returns
        -------
        list
            A list of ``FrozenJSON`` objects built from ``User`` objects, if
            get_cursor is True, returns a tuple with (list, cursor, more flag)

        Raises
        ------
        Forbidden
            If the current user does not have permission to list users


        Notes
        -----
        See this for documentation on how to build filter and order
        expressions: https://cloud.google.com/appengine/docs/python/ndb/queries

        """
        ensure(self._user, 'LIST', self.MODEL)

        # Try to query, if a BadQuery error is raised, the user
        # does not have assigned users (in case is executive or supervisor),
        # catch the error and return an empty list
        # try:
        #     q = self._user.list_query(self.MODEL, namespace=self._namespace)
        # except EmptyAssignments:
        #     return []
        q = self._user.list_query(self.MODEL, namespace=self._namespace)

        if order:
            q = q.order(*order)

        if filter:
            q = q.filter(*_wrap_in_list(filter))

        # if cursor is a string or None, fetch the data using fetch_page
        # and pass the cursor
        if get_cursor:
            # If the user passed start in cursor, it means we should build
            # the cursor for the first time
            cursor = Cursor(urlsafe=cursor)
            users, next_c, more = q.fetch_page(count, start_cursor=cursor)
            dicts = [FrozenJSON(o.to_dict()) for o in users]
            next_ = next_c.urlsafe() if next_c else None
            return dicts, next_, more
        # Otherwise use cursor as offset
        else:
            # Query the datastore
            users = q.fetch(limit=count, offset=offset)
            dicts = [FrozenJSON(o.to_dict()) for o in users]
            return dicts

    @validate_namespace_in_keys
    @parse_and_validate_urlsafe_keys_in_args_with_model('other_key')
    def read(self, other_key=None, email=None):
        """
        Read a user profile using id or email.


        Parameters
        ----------
        other_key: Key or urlsafe Key
            The id for the user whose profile wants to be read
        email: str
            The email for the user whose profile wants to be read,
            case-insensitive

        Returns
        -------
        user: FrozenJSON
            A FrozenJSON instance with the requested user information

        Raises
        ------
        Forbidden
            If the current user does not have access to read the user profile
            with the given id
        NotFound
            If it doesn't exist a user with the given id or email
        """
        # Convert e-mail to lowercase
        if email:
            email = email.lower()

        self.logger.debug('Reading user. Key: {}. Email: {}'.format(other_key,
                                                                    email))

        if (other_key and email) or (not other_key and not email):
            raise ValueError("Cannot call with other_key and email")

        if other_key:
            other = other_key.get()

        if email:
            other = self.MODEL.query(self.MODEL.email == email,
                                     namespace=self._namespace).get()

        if other:
            ensure(self._user, READ, other)
            return FrozenJSON(other.to_dict())
        else:
            raise NotFound(message="No user found",
                           user_message=u"No se encontró al usuario")

    def read_me(self):
        """
        Reads the current user data

        Returns
        -------
        user: FrozenJSON
            A FrozenJSON instance with the current user information

        Raises
        ------
        Forbidden
            If the current user does not have access to read the user profile
        NotFound
            If it doesn't exist a user with the current key
        """
        return self.read(other_key=self.key)

    @validate_namespace_in_keys
    @parse_and_validate_urlsafe_keys_in_args('other_key', MODEL)
    def delete(self, other_key):
        """
        Delete a user with a given id


        Parameters
        ----------
        other_key: Key or urlsafe Key
            The id for the user whose account wants to be deleted

        Returns
        -------
        success: bool
            Returns True if the operation was successful

        Raises
        ------
        Forbidden
            If the user does not have permission to perform the operation
        NotFound
            If a user with the given id does not exist

        """
        other = other_key.get()
        # If there's something in the database, continue
        if other:
            # Check permissions
            ensure(self._user, DELETE, other)
            # If it has enough permissions, delete
            other.key.delete()
            return True
        # If not, raise an exception
        else:
            raise NotFound("No user found for key {}".format(other_key),
                           u"No se encontró al usuario")

    @property
    def ndb_model(self):
        """
        Returns the underlying datastore model for the current user. Use this
        only if you need access to the plain datastore model.


        Returns
        -------
        user: User
            Current user model

        """
        return self._user

    @property
    def key(self):
        """
        Get the key for the current user.


        Returns
        -------
        key: Key
            Key (urlsafe) for the current user

        """
        return self._user.key.urlsafe()

    @property
    def namespace(self):
        """
        Returns the namespace for the user
        """
        return self._user.key.namespace()

    def can(self, action, object_key):
        key = Key(urlsafe=object_key)
        obj = key.get()
        return can(self._user, action, obj)

    @classmethod
    def load_with_email(cls, email):
        """
        Used to load a user without password (for password reset)
        user must be active

        Parameters
        ----------
        email : str
            Email. Case-insensitive

        Raises
        ------
        NotFound
            If there's no user with such email
        BadRequest
            If user status is not active

        Returns
        -------
        UserGuard
            Guard for the user
        """
        email = email.lower()

        # Check Person for someone with such email
        user = cls.MODEL.query(cls.MODEL.email == email, namespace=None).get()

        cls._validate_active_user(user)

        return cls(user=user)

    @classmethod
    def load_with_token(cls, token, status, token_type):
        """Load user with signup token, user must be pending

        Parameters
        ----------
        token: str
            Token
        status: str
            Expected status for the user
        token_type: str
            Expected token type

        Raises
        ------
        InternalServerError
            If valid token but no user in the database, token does not have
            the expected type or if loaded user not have the expected status
        Forbidden
            If invalid or expired token
        """
        decoded_token = decode(token)

        decoded_token_type = decoded_token.get('type')

        if decoded_token_type != token_type:
            raise InternalServerError('Decoded token has type {}, expected {}'
                                      .format(decoded_token_type, token_type),
                                      u'No se ha podido validar el token')

        user_key = decoded_token['user_key']
        user = Key(urlsafe=user_key).get()

        if not user:
            message = 'A valid token was generated but no user in the db'
            logger.error(message)
            raise InternalServerError(message,
                                      'No se ha podido verificar el token')

        if user.status != status:
            msg = ('Invalid token, status should be {} but it is {}'
                   .format(status, user.status))
            user_message = (u'Token inválido, el status de la cuenta no '
                            u'permite realizar la acción requerida')
            raise InternalServerError(msg, user_message)

        return cls(user=user)

    @classmethod
    def load_with_login(cls, email, password, namespace=None):
        '''
        Verify login data for an account.

        Parameters
        ----------
        email: str
            Email for the user. Case-insensitive
        password: str
            Password for the user
        namespace: str
            This determines which namespace to use for querying the datastore

        Returns
        -------
        UserGuard
            Returns a ``UserGuard`` instance with the user if the login
            data is correct

        Raises
        ------
        NotFound
            When the email does not exist in the datastore
        Unauthorized
            When the email exists but the password is wrong

        '''
        email = email.lower()

        # try to get person with such email
        user = cls.MODEL.query(cls.MODEL.email == email,
                               namespace=namespace).get()

        cls._validate_active_user(user)

        # get hashed password and compare
        hashed = user.password.encode('utf-8')
        correct_password = sha256_crypt.verify(password, hashed)

        if correct_password:
            return cls(user=user)
        else:
            logger.error("Can't load profile: password does not match")
            raise Unauthorized("Cannot login, Password does not match",
                               u'No se ha podido iniciar sesión')

    @classmethod
    def set_password_with_signup_token(cls, token, password):
        """
        Activates an account for a new user. User.status must be pending

        Parameters
        ----------
        token: str
            The token generated for the new user
        password: str
            The password for the new account

        Returns
        -------
        user: UserGuard
            A UserGuard instance for the new user

        Raises
        ------
        BadRequest
            If the password does not have at least 6 characters
        InternalServerError
            If valid token but no user in the database, if token is not a
            signup token or if loaded user not have pending status
        Forbidden
            If invalid or expired token
        """
        user = cls.load_with_token(token, status=UserStatus.Pending,
                                   token_type=TokenType.SignUp)
        user = user.update_me(password=password,
                              status=UserStatus.Active)
        return user

    def update_me(self, **kwargs):
        """
        Update the profile for the current person


        Parameters
        ----------
        **kwargs
            Arguments to be updated for the current person profile
            these arguments must be valid ``User`` properties.

        Returns
        -------
        person: FrozenJSON
            A FrozenJSON representation with the updated person data

        Raises
        ------
        Forbidden
            If the user does not have permission to perform the operation

        Notes
        -----
        The names and types in kwargs must match the corresponding Person
        model, the only exception are KeyProperties, for these type of
        properties you can pass either a Key or just the urlsafe key
        for the object and the object will internally build the Key from the
        string.
        """
        return self.update(other_key=self.key, **kwargs)

    # TODO: validate namespace?
    @parse_urlsafe_keys_in_kwargs_with_model
    @parse_and_validate_urlsafe_keys_in_args_with_model('other_key')
    def update(self, other_key, **kwargs):
        """
        Updates the profile information for a person with a given id

        Parameters
        ----------
        other_key: Key or urlsafe Key
            The Key for the person whose information wants to be changed

        Returns
        -------
        person: FrozenJSON
            A FrozenJSON representation with the updated person data


        Raises
        ------
        Forbidden
            If the user does not have permission to perform the operation
        ResourceNotFound
            If a person with the given key does not exist
        BadRequest
            If there was something wrong with the input data

        Notes
        -----
        The names and types in kwargs must match the corresponding Person
        model, the only exception are KeyProperties, for these type of
        properties you can pass either a Key or a urlsafe Key for the object
        and the object will internally build the Key from the string.
        """

        # Validate input
        kwargs = validate_input_form(kwargs, self.MODEL, check_required=False)

        other = copy(other_key.get())

        # if updating password, verify length and hash again
        if 'password' in kwargs:

            if len(kwargs['password']) < MIN_PASSWORD_LENGTH:
                raise BadRequest('Password must me at least {} characters'
                                 .format(MIN_PASSWORD_LENGTH),
                                 u'El password debe tener al menos {} '
                                 u'caracteres'.format(MIN_PASSWORD_LENGTH))

            kwargs['password'] = sha256_crypt.encrypt(kwargs['password'])

        if other:
            for k, v in kwargs.items():
                setattr(other, k, v)

            ensure(self._user, UPDATE, other)
            other.put()

            return FrozenJSON(other.to_dict())
        else:
            raise NotFound

    def generate_restore_password_token(self):
        """Generate token to restore password
        """
        return generate(user_key=self.key,
                        type=TokenType.RestorePassword)

    @classmethod
    def set_password_with_restore_token(cls, token, password):
        """Restore password with token. User must be active

        Parameters
        ----------
        token: str
            The token generated for the new user
        password: str
            The new password for the account

        Returns
        -------
        FrozenJSON
            A frozen representation of the user

        Raises
        ------
        InternalServerError
            If valid token but no user in the database, if token is not a
            restore password token or if loaded user not have active status
        Forbidden
            If invalid or expired token
        """
        guard = cls.load_with_token(token, UserStatus.Active,
                                    TokenType.RestorePassword)
        user = guard.update_me(password=password)
        return user

    @classmethod
    def bootstrap_account(cls, namespace=None, **kwargs):
        """
        Bootstrap an account. Use this to create the first account in the app,
        and create the rest of the accounts using Userguard.create_account.
        Do not expose this method in your app as this one can create any type
        of accounts without checking permissions or generating tokens.

        Parameters
        ----------
        namespace: str
            Which namespace to use for creating the account

        **kwargs
            The arguments sent to the User constructor, these parameters
            should be any valid User property. See the User model to check
            the properties names and types. Do not include status as this will
            be set to `active`

        Returns
        -------
        UserGuard
            A UserGuard instance

        Raises
        ------

        Notes
        -----
        The names and types in kwargs must match the corresponding User model,
        the only exception are KeyProperties, for these type of properties you
        can pass either a Key or a urlsafe Key for the object.
        """
        if 'status' in kwargs:
            raise BadRequest('Do not include status as this will be set to'
                             ' active')

        # Create Person
        person = cls.MODEL.query(cls.MODEL.email == kwargs['email'],
                                 namespace=namespace).get()
        if person:
            raise BadRequest('User with such email already exists')

        # TODO: validate password size

        # hash password
        kwargs['password'] = sha256_crypt.encrypt(kwargs.get('password'))

        # create user
        user = cls.MODEL(status=UserStatus.Active, **kwargs)
        user.put()

        return cls(user)

    @classmethod
    def _validate_active_user(cls, user):
        if not user:
            raise NotFound("Coulndn't load user, datastore returned {}"
                           .format(user),
                           u'No se ha encontrado el usuario')

        if user.status == UserStatus.Pending:
            raise BadRequest('User status is pending',
                             u'No se ha podido realizar la operación '
                             u'la cuenta está marcada como pendiente')

        if user.status != UserStatus.Active:
            raise BadRequest('User status is not active',
                             u'No se ha podido realizar la operación'
                             u' la cuenta no se encuentra activa')

        return True
