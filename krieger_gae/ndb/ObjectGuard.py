import logging
from ..exceptions import NotFound
from .input import (parse_urlsafe_keys_in_kwargs_with_model,
                    parse_and_validate_urlsafe_keys_in_args_with_model,
                    validate_input_form, validate_namespace_in_keys)
from .helpers import _wrap_in_list, _is_collection
from .FrozenJSON import FrozenJSON
from .introspection import DatastoreModelIntrospector
# from ..modules.notifications.center import NotificationCenter

from bouncer import can
from .helpers import ensure
from bouncer.constants import CREATE, READ, UPDATE, DELETE
from google.appengine.datastore.datastore_query import Cursor
from google.appengine.ext.ndb.key import Key
from google.appengine.ext import ndb


class ObjectGuard(object):
    """
    This object is built on top of the datastore model, it provides
    with generic methods to perform CRUD operations on any model and also
    (optionally) checks for permissions for every operation.

    This is an abstract class and should not be used directly, instead
    use the concrete classes for the objects in the datastore (e.g.
    RequestGuard, AppointmentGuard, etc.) but use this as a reference.

    Attributes
    ----------
    namespace: str
        Namespace used for all the operations, trying to perform operations
        outside this namespace will lead to a Forbidden exception. Use 'local'
        to use the same namespace where the user used for instantiating the
        guard is located
    model: ndb.Model
        The model that will be used to query the datastore

    Parameters
    ----------
    userguard: UserGuard
        A ``UserGuard`` instance for the user performing the operations,
        for example, for creating a RequestGuard for the currently logged
        in user: ``rg = RequestGuard(current_user)``, this object is used
        to check the permissions for every operation performed.
    notifier: Object
        An object that will be notified on create, update and delete operations
        the object must have create, update and delete instance methods with
        a data parameter
    check_permissions: bool
        Whether to check permissions on every operation
    model: Datastore model
        The model used to perform operations, if None it checks for a MODEL
        class variable

    Notes
    -----

    Examples
    --------
    ObjectGuard that uses the Post model

    .. code-block:: python

        class PostGuard(ObjectGuard):
            MODEL = Post

    ObjectGuard that uses the Post model and stores it in the 'client'
    namespace

    .. code-block:: python

        class PostGuard(ObjectGuard):
            MODEL = Post
            NAMESPACE = 'client'


    """
    MODEL = None
    NAMESPACE = 'local'

    def __init__(self, userguard, notifier=None,
                 check_permissions=True, model=None):
        self._guard = userguard
        self._user = userguard._user
        self._notifier = notifier
        self._check_permissions = check_permissions

        self._model = model if model else self.__class__.MODEL

        if self.NAMESPACE == 'local':
            self._namespace = self._user.key.namespace()
        else:
            self._namespace = self.NAMESPACE

        self.logger = logging.getLogger(__name__)

    # TODO: parse keys to urlsafe keys in data
    def create_multi(self, data):
        """
        Create multiple objects in the datastore

        Parameters
        ----------
        data : iterable
            Iterable containing the data to be saved in the datastore

        Returns
        -------
        list
            A list of FrozenJSON objects

        Raises
        ------
        Forbidden
            If the user does not have permission to perform the operation
        BadInput
            If the input is not in the right format

        Notes
        -----
        All objects are validated in content and permissions before saving into
        the datastore, if any of these fails, no object will be saved
        """
        # Validate and parse all objects
        data = [validate_input_form(obj, self._model, check_required=True) for
                obj in data]
        # Instantiate objects
        objs = [self._model(namespace=self._namespace, **obj) for obj in data]

        # Check permissions
        if self._check_permissions:
            for obj in objs:
                ensure(self._user, CREATE, obj)

        # Save objects
        ndb.put_multi(objs)

        frozen_objs = [FrozenJSON(obj.to_dict()) for obj in objs]

        # Send notification
        if self._notifier:
            for kwargs, frozen in zip(data, frozen_objs):
                data = dict(kwargs=kwargs, result=frozen,
                            origin=FrozenJSON(self._user.to_condensed_dict()),
                            model=self._model.__name__)
                self._notifier.create(data=data)

        # Create notifications
        return frozen_objs

    @parse_urlsafe_keys_in_kwargs_with_model
    def create(self, **kwargs):
        """
        Create a new model in the datastore

        Parameters
        ----------
        **kwargs
            These parameters must be valid properties for the model,
            e.g. if you want to create a Request using RequestGuard,
            the parameters must be present in the Request ndb model
            definition

        Returns
        -------
        FrozenJSON
            A FrozenJSON instance with the created object

        Raises
        ------
        Forbidden
            If the user does not have permission to perform the operation
        BadInput
            If the input is not in the right format

        Notes
        -----
        The names and types in kwargs must match the corresponding User model,
        the only exception are KeyProperties, for these type of properties you
        can pass either a Key or just a urlsafe key for the object and
        the object will internally build the Key from the urlsafe key.

        Properties are automatically casted (e.g. DateProperty can be
        casted from an iso string, StructuredProperty can be casted from
        a dict), see :meth:`ndb.input.validate_input_form` for supported types

        """
        # Validate input
        kwargs = validate_input_form(kwargs, self._model, check_required=True)

        # Create object, we do this before checking permissions
        # because the rule for creation may depend on the contents
        # of the object. (e.g. a user can only create requests when the
        # user.request value is themselves)
        obj = self._model(namespace=self._namespace, **kwargs)

        # Check permissions
        if self._check_permissions:
            ensure(self._user, CREATE, obj)

        # Save it in the db
        obj.put()

        frozen = FrozenJSON(obj.to_dict())

        # Send notification
        if self._notifier:
            data = dict(kwargs=kwargs, result=frozen,
                        origin=FrozenJSON(self._user.to_condensed_dict()),
                        model=self._model.__name__)
            self._notifier.create(data=data)

        return frozen

    def list(self, count=10, offset=0, cursor=None, filter_=None, order=None,
             get_cursor=False, raw=False):
        """
        List models

        Parameters
        ----------
        count: int
            Maximum number of elements to return. If None it returns all
            entities that matched the criteria (only valid when get_cursor
            is False)
        offset: int
            Number of elements to skip
        cursor: str
            urlsafe cursor to continue querying
        get_cursor: boolean
            if this is True, the function will return the objects, a cursor
            and a flag indicating whether there are more objects. Ignores
            offset if True.
        filter_: Filter
            Filter to apply, must be a valid Filter NDB expression(s)
        order: Order
            How to sort the results, must be a valid Order NDB expression if no
            order is passed, self.DEFAULT_ORDER will be used if exists
        raw: boolean
            Returns entity objects instead of FrozenJSON

        Returns
        -------
        objects: list
            A list with FrozenJSON instances built from the model or entities
            if raw is True

        Raises
        ------
        Forbidden
            If the user does not have permission to perform the operation

        Notes
        -----
        See this for documentation on how to build filter and order
        expressions: https://cloud.google.com/appengine/docs/python/ndb/queries

        """
        if self._check_permissions:
            ensure(self._user, 'LIST', self._model)

        # TODO: fix this
        # try:
        #     q = self._user.list_query(self._model, namespace=self._namespace)
        # except EmptyAssignments:
        #     return []
        q = self._user.list_query(self._model, namespace=self._namespace)

        # Check if the user sent an order expression
        if order:
            q = q.order(*order)
        # If not, check if the current guard has a DEFAULT_ORDER property
        elif self._default_order:
            self.logger.debug('Setting order to DEFAULT_ORDER: %s',
                              self._default_order)
            q = q.order(*self._default_order)

        if filter_:
            q = q.filter(*_wrap_in_list(filter_))

        self.logger.debug('Query: %s', q)

        # if cursor is a string or None, fetch the data using fetch_page
        # and pass the cursor
        if get_cursor:
            cursor = Cursor(urlsafe=cursor)
            res, next_c, more = q.fetch_page(count, start_cursor=cursor)
            objs = res if raw else [FrozenJSON(o.to_dict()) for o in res]
            next_ = next_c.urlsafe() if next_c else None
            return objs, next_, more
        # Otherwise use cursor as offset
        else:
            # Query the datastore
            res = q.fetch(limit=count, offset=offset)
            objs = res if raw else [FrozenJSON(o.to_dict()) for o in res]
            return objs

    @validate_namespace_in_keys
    @parse_and_validate_urlsafe_keys_in_args_with_model('other_key')
    def read(self, other_key):
        """
        Read a model with a given key

        Parameters
        ----------
        other_key: str
            The key for the object that wants to be read

        Returns
        -------
        object: FrozenJSON
            A FrozenJSON instances with the retrieved object data

        Raises
        ------
        Forbidden
            If the user does not have permission to perform the operation

        NotFound
            If there's no object with the given key
        """
        # Load from db
        other = other_key.get()

        # If there's something in the database, check permissions
        if other:
            if self._check_permissions:
                ensure(self._user, READ, other)

            return FrozenJSON(other.to_dict())

        # If not, raise an exception
        else:
            raise NotFound

    @validate_namespace_in_keys
    @parse_urlsafe_keys_in_kwargs_with_model
    @parse_and_validate_urlsafe_keys_in_args_with_model('other_key')
    def update(self, other_key, append=True, **kwargs):
        """
        Update a model with a given key

        Parameters
        ----------
        other_key: str
            The key for the object that wants to be updated
        append: bool
            If append is True, values passed for repeadted properties in
            kwargs will be appended instead of replaced
        **kwargs
            These parameters must be valid properties for the model,
            e.g. if you want to create a Request using RequestGuard,
            the parameters must be present in the Request ndb model
            definition

        Returns
        -------
        object: FrozenJSON
            A FrozenJSON instances with the updated object

        Raises
        ------
        Forbidden
            If the user does not have permission to perform the operation
        BadInput
            If the input is not in the right format
        NotFound
            If there's no object with the given key

        Notes
        -----
        The names and types in kwargs must match the corresponding User model,
        the only exception are KeyProperties, for these type of properties you
        can pass either a Key or just the key for the object and the object
        will internally build the Key from the key.

        """
        # Validate input
        kwargs = validate_input_form(kwargs, self._model, check_required=False)

        # Load from db
        other = other_key.get()

        # If there's something in the database, check permissions
        if other:
            insp = DatastoreModelIntrospector(self._model)

            # Update locally
            for k, v in kwargs.items():
                if k in insp.repeated_properties() and append:
                    # If property is repeated and appendwas true, append
                    # contents instead of replacing
                    contents = getattr(other, k)
                    contents.extend(v)
                    setattr(other, k, contents)
                else:
                    # Otherwise just replace the value
                    setattr(other, k, v)

            # Check it has permissions for the changes made
            if self._check_permissions:
                ensure(self._user, UPDATE, other)

            # If it has enough permissions, update and save
            other.put()

            frozen = FrozenJSON(other.to_dict())

            # Send notification
            if self._notifier:
                data = dict(kwargs=kwargs, result=frozen,
                            origin=FrozenJSON(self._user.to_condensed_dict()),
                            model=self._model.__name__)
                self._notifier('update', data=data)

            return frozen
        # If not, raise an exception
        else:
            raise NotFound

    @validate_namespace_in_keys
    @parse_and_validate_urlsafe_keys_in_args_with_model('other_key')
    def delete(self, other_key, delete_children=False):
        """
        Delete a model with a given key

        Parameters
        ----------
        other_key: str
            The key (or list of keys) for the object(s) to delete

        Returns
        -------
        success: bool
            Returns True if the operation was successful

        Raises
        ------
        Forbidden
            If the user does not have permission to perform the operation
        NotFound
            If there's no object with the given key
        """

        # Force it to be a collection
        if not _is_collection(other_key):
            other_key = [other_key]

        # Load from db and check permissions
        other = [o.get() for o in other_key]

        # If there's something in the database, check permissions
        if all(other):

            if self._check_permissions:
                for o in other:
                    ensure(self._user, DELETE, o)

            # If it has enough permissions, delete
            ndb.delete_multi(other_key)

            # Delete children if needed
            if delete_children:
                for k in other_key:
                    ndb.delete_multi(ndb.Query(ancestor=k)
                                     .iter(keys_only=True))

            if self._notifier:
                for o in other:
                    origin = self._user.to_condensed_dict()
                    data = dict(result=FrozenJSON(o.to_dict()),
                                origin=FrozenJSON(origin),
                                model=self._model.__name__)
                    self._notifier('delete', data=data)

            return True

        # If not, raise an exception
        else:
            raise NotFound("Couldn't delete object(s) because some don't "
                           "exist (key.get() returned None)")

    def validate(self, data, check_required=True):
        """
        Validate the input data (a dictionary) against the datastore model
        assigned to the ObjectGuard

        Parameters
        ----------
        data : dict
            Dictionary containing the data to be validated

        check_required : boolean, optional
            Check that all required properties in the model exist in data,
            defaults to True


        Raises
        ------
        BadInput
            When check_required is True and required properties do not exist
            in data

        """
        return validate_input_form(data, self._model,
                                   check_required=check_required)

    @property
    def ndb_model(self):
        return self._model

    @property
    def _default_order(self):
        """Returns self.DEFAULT_ORDER if exists, otherwise returns None
        """
        if hasattr(self, 'DEFAULT_ORDER'):
            return self.DEFAULT_ORDER
        else:
            return None

    def can(self, action, object_key):
        key = Key(urlsafe=object_key)
        obj = key.get()
        return can(self._user, action, obj)
