import unittest

from krieger_gae.ndb.input import list_with_request_args
from krieger_gae.testing import SingleDatastoreTestCase
from .dummy import MyUserGuard, Guard


class TestListWithRequestArgs(SingleDatastoreTestCase):

    @classmethod
    def setUpClass(cls):
        super(TestListWithRequestArgs, cls).setUpClass()

        cls.guard = MyUserGuard.bootstrap_account(password='mypassword',
                                                  email='myemail@krieger.mx')

        # Create more accounts
        for i in range(5):
            MyUserGuard.bootstrap_account(password='password',
                                          email='dude{}@krieger.mx'.format(i),
                                          role='admin')

    def test_can_use_count(self):
        res = list_with_request_args(dict(count=1), self.guard)
        assert len(res) == 1

    def test_can_use_offset(self):
        res = list_with_request_args(dict(count=1, offset=1), self.guard)
        assert len(res) == 1

    def test_can_get_cursor(self):
        data = dict(get_cursor=True, count=1)
        res, cursor, more = list_with_request_args(data, self.guard)
        assert res and cursor and more

    def test_can_user_cursor_to_retrieve_more_elements(self):
        data = dict(get_cursor=True, count=1)
        res, cursor, more = list_with_request_args(data, self.guard)
        data = dict(cursor=cursor, count=1, get_cursor=True)
        res, cursor, more = list_with_request_args(data, self.guard)
        assert res and cursor and more

    def test_can_filter(self):
        res = list_with_request_args(dict(email='dude0@krieger.mx'),
                                     self.guard)
        assert len(res) == 1

    def test_can_filter_multiple_values(self):
        res = list_with_request_args(dict(email=['dude0@krieger.mx',
                                                 'dude1@krieger.mx']),
                                     self.guard)
        assert len(res) == 2

    def test_can_filter_multiple_keys(self):
        data = dict(email='dude0@krieger.mx', role='admin')
        res = list_with_request_args(data, self.guard)
        assert len(res) == 1

    def test_can_order(self):
        data = dict(order=['-email', '-role'])
        res = list_with_request_args(data, self.guard)
        assert res[0].email == 'myemail@krieger.mx'

    def test_can_order_with_single_parameter(self):
        data = dict(order='-email')
        res = list_with_request_args(data, self.guard)
        assert res[0].email == 'myemail@krieger.mx'
