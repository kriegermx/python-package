import re

from ..exceptions import BadRequest


def email_validator(prop, value):
    """Email validator

    You can use this function as a validator in a ndb.StringProperty by using
    the validator parameter

    Raises
    ------
    BadRequest
        If the string does not match the right format

    Notes
    -----
    Based on https://www.scottbrady91.com/Email-Verification/Python-Email-Verification-Script
    # noqa
    """
    match = re.match(('^[_a-z0-9-]+([._a-z0-9-+]+)*@[_a-z0-9-]'
                      '+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$'), value)

    if not match:
        msg = 'String does not match email format: {}'.format(value)
        raise BadRequest(msg)
