Testing
=======

This module provides subclasses of `unittest.TestCase` that mock a Datastore. 
If you need to mock more Google Cloud Services see this:
https://cloud.google.com/appengine/docs/standard/python/tools/localunittesting

.. automodule:: krieger_gae.testing
    :members: