"""Tools for integrating Google Cloud Endpoints with Flask-login
"""
import hashlib
import logging
import os
import Cookie
from functools import wraps

from .exceptions import Unauthorized
from . import settings
from .ndb.UserGuard import UserGuard

from flask.sessions import TaggedJSONSerializer
from itsdangerous import URLSafeTimedSerializer, BadSignature


def _decode_flask_cookie(secret_key, cookie_str):
    """Decode a flask cookie

    Parameters
    ----------
    secret_key: str
        Cookies secret key

    cookiet_str: str
        Cookie

    Notes
    -----
    Taken from Taken from: https://gist.github.com/babldev/502364a3f7c9bafaa6db
    """
    salt = 'cookie-session'
    serializer = TaggedJSONSerializer()
    signer_kwargs = {
        'key_derivation': 'hmac',
        'digest_method': hashlib.sha1
    }
    s = URLSafeTimedSerializer(secret_key, salt=salt, serializer=serializer,
                               signer_kwargs=signer_kwargs)
    return s.loads(cookie_str)


def auth_required(func):
    """Required authentication for an incoming request

    Use this decorator to request authentication in a cloud endpoint. Before
    using this you should set the FLASK SECRET KEY (the one called
    SECRET_KEY in flask config)  like this

    .. code-block:: python

        import krieger_gae

        krieger_gae.set_flask_secret_key('my-flask-secret')

    Raises
    ------
    Unauthorized
        If the user is not authenticated

    Examples
    --------

    .. literalinclude:: ../examples/endpoints_auth.py

    """
    @wraps(func)
    def func_wrapper(self, *args, **kwargs):
        # Check if cookie is valid
        cookie_string = os.environ.get('HTTP_COOKIE')
        cookie = Cookie.SimpleCookie()
        try:
            cookie.load(cookie_string)
        except AttributeError:
            # There is no cookie, which means the user hasn't auth
            logging.info('Someone called the api without a cookie...')
            raise Unauthorized('You need to authenticate first')

        try:
            session = cookie['session'].value
        except KeyError:
            # Cookie exists but doesnt have a session value
            logging.info('Someone called the api with a cookie but without a '
                         'session value')
            raise Unauthorized('You need to authenticate first')

        try:
            SECRET_KEY = settings('SECRET_KEY')
            decoded = _decode_flask_cookie(SECRET_KEY, session)
        except BadSignature:
            # The cookie hads been tampered
            logging.info('Someone called the API with a cookie that does not '
                         'match the signature')
            raise Unauthorized('You need to authenticate first')

        try:
            user_id = decoded['user_id']
            current_user = UserGuard.load_profile_with_id(user_id)
        except KeyError:
            # The cookie was decoded but it does not have a user_id field
            logging.info('Someone called the API with a valid cookie that does'
                         ' not have a valid user_id value')
            raise Unauthorized('You need to authenticate first')

        # Finally, even if all of this passes, check if you got a valid
        # user, it may be the case that the cookie is valid but the user
        # does not exists (e.g. if the user was deleted)
        if not current_user:
            raise Unauthorized('You need to authenticate first')

        logging.info('API Auth ok')
        return func(self, *args, current_user=current_user, **kwargs)

    return func_wrapper
