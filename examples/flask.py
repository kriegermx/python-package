import logging

from flask import Blueprint, request, redirect, url_for, flash, render_template
from flask_login import current_user, login_required

from .guard import PostGuard
from krieger_gae.exceptions import BaseException, Forbidden, BadRequest


routes = Blueprint('posts', __name__, url_prefix='/posts')

logger = logging.getLogger(__name__)


@routes.route('', methods=['GET'])
@login_required
def list():
    pg = PostGuard(current_user)

    # You can register an error handler for BaseException in flask
    # so any errors raise here are rendered in an appropriate template
    posts = pg.list()

    return render_template('posts/list.html', posts=posts)


@routes.route('/<key>', methods=['GET'])
@login_required
def read(key):
    pg = PostGuard(current_user)

    # Alternatively you can catch errors here to show them as flash messages
    try:
        post = pg.read(key)
    except BaseException as e:
        flash(e.message)
        return redirect(url_for('.list'))

    return render_template('posts/detail.html', post=post)


@routes.route('/create', methods=['GET', 'POST'])
@login_required
def create():
    if request.method == 'POST':
        pg = PostGuard(current_user)

        # Or you can selectively catch some errors and leave some for the
        # error handler
        try:
            key = pg.create(**request.form).key
        except (Forbidden, BadRequest) as e:
            flash(e.message)
            return redirect(url_for('.create'))

        # Send a confirmation message
        flash('Se ha creado un post')

        return redirect(url_for('.read', key=key))
    else:
        return render_template('posts/create.html')


@routes.route('/<key>/update', methods=['GET', 'POST'])
@login_required
def update(key):
    if request.method == 'GET':
        pg = PostGuard(current_user)
        post = pg.read(key)
        return render_template('posts/update.html', post=post)
    else:
        pg = PostGuard(current_user)

        try:
            pg.update(key, **request.form)
        except BaseException as e:
            flash(e.message)
            return redirect(url_for('.update', key=key))
        else:
            flash('Se ha actualizado el post')
            return redirect(url_for('.read', key=key))


@routes.route('/<key>/delete', methods=['GET'])
@login_required
def delete(key):
    pg = PostGuard(current_user)

    try:
        pg.delete(key)
    except BaseException as e:
        flash(e.message)
        return redirect(url_for('.read', key=key))
    else:
        flash('Se ha borrado el psot')
        return redirect(url_for('.list'))
