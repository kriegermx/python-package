from google.appengine.ext import ndb


class User(ndb.Model):
    """This is an example User class. The class should have at minimum the
    following properties

    Attributes
    ----------
    password: ndb.StringProperty
        User's password (a hashed version is stored)
    email: ndb.StringProperty
        User's e-mail
    status: ndb.StringProperty
        A flag indicating the user's status. You can define your own statuses
        but at minimum it should have `pending` and `active` as defined in
        krieger_gae.ndb.constants.UserStatus
    """
    password = ndb.StringProperty()
    email = ndb.StringProperty(required=True)
    role = ndb.StringProperty()
    status = ndb.StringProperty()

    def list_query(self, model, namespace):
        """
        For your user class you need to provide a list_query method, this
        can be useful if for example, different types of user have different
        permissions for listing: admins can see everything but users can only
        see objects associated with them. If no such thing is needed, just
        return a generic query. This function should accept the following
        parameters

        Parameters
        ----------
        model: ndb.Model
            The model to query
        namespace: str
            The namespace to be used in the query
        """
        return model.query()

    def to_dict(self):
        """
        UserGuard will call user.to_dict() for every model you query,
        so make sure you make any non-serializable property a serializable
        one
        """
        d = super(User, self).to_dict()

        # Add a  urlsafe key to the user representation
        d['key'] = self.key.urlsafe()
        return d


class Post(ndb.Model):
    """Sample model for a post
    """
    content = ndb.StringProperty(required=True)
    tag = ndb.StringProperty(repeated=True)
    user = ndb.KeyProperty(required=True)
    timestamp = ndb.DateTimeProperty(auto_now_add=True)

    def to_dict(self):
        d = super(Post, self).to_dict()
        d['key'] = self.key.urlsafe()
        # convert your datetime object to a string to it can be sent via json
        d['timestap'] = convert_to_iso8601(self.timestap)
        return d
