UserGuard
=========

This class provides several functions to perform CRUD operations on users,
which are created first by generating a token and then by using that token
to active the account and set the password.

Before using this class, make sure you have set the token secret:


.. code-block:: python

    import krieger_gae

    krieger_gae.set_token_secret('my-token-secret')


.. automodule:: krieger_gae.ndb.UserGuard
    :members: