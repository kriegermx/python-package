class Enum(object):
    """Simple class for enumeration

    A Python class for object enumeration. Sample usage:

    class Color(Enum):
        Red, Blue, Yellow = range(3)

    # Return available choices (variables must start with an uppercase char)
    Color.choices()

    """
    @classmethod
    def choices(cls):
        keys = cls.__dict__.keys()
        keys = {k for k in keys if k[0].isupper()}
        return {v for k, v in cls.__dict__.items() if k in keys}
