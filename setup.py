import re
import ast
from setuptools import setup, find_packages

_version_re = re.compile(r'__version__\s+=\s+(.*)')

with open('krieger_gae/__init__.py', 'rb') as f:
    VERSION = str(ast.literal_eval(_version_re.search(
        f.read().decode('utf-8')).group(1)))

setup(name='krieger-gae',
      packages=find_packages(exclude=['tests']),
      version=VERSION,
      description=('Utilities for Google App Engine'),
      url='http://bitbucket.org',
      author='Eduardo Blancas Reyes',
      author_email='eduardo.br@krieger.mx',
      keywords=['app_engine', 'gae'],
      install_requires=[
        'bouncer',  # for permissions
        'itsdangerous',  # for token generation
        'passlib',  # for passwords
        'sendgrid', # for high volume mailing
      ],
      scripts=['bin/constants'],
      package_data={'krieger_constants': ['templates/*']},
      )
