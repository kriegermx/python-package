#!/usr/bin/env python
import os
import json
import yaml
from jinja2 import Environment, PackageLoader, select_autoescape


def make_value(value, type_key, constant_key):
        """Make value based on constants info
        """
        return '{}{}.{}'.format(type_key[0].lower(), constant_key[0].lower(),
                                value.lower())


def _process_constants(path_to_constants):
    """Process constants.yaml

    Parameters
    ----------
    path_to_constants: str
        constants.yaml location
    """
    with open(path_to_constants) as f:
        cons = yaml.load(f)

    def compute_key_vals(values, type_key, constant_key):
        return [dict(name=value, value=make_value(value,
                                                  type_key, constant_key))
                for value in values]

    # Compute constant name and convert to list of dicts
    return [dict(name=type_key+constant_key,
                 constants=compute_key_vals(values, type_key, constant_key))
            for type_key, constants in cons.items()
            for constant_key, values in constants.items()]


def _process_locale(path_to_constants, path_to_locale):
    """Process {locale}.yaml

    Parameters
    ----------
    path_to_constants: str
        constants.yaml location
    path_to_locale: str
        Folder where the {locale}.yaml files is located
    """
    with open(path_to_constants) as f:
        cons = yaml.load(f)

    with open(os.path.join(path_to_locale)) as f:
        locale = yaml.load(f)

    locale = {type_key+constant_key: values
              for type_key, constants in locale.items()
              for constant_key, values in constants.items()}

    def compute_id_translation(type_key, constant_key, values):
        name = type_key+constant_key
        return [dict(id=make_value(value, type_key, constant_key),
                     translation=locale[name][value])
                for value in values]

    return [dict(name=type_key+constant_key,
                 constants=compute_id_translation(type_key, constant_key,
                                                  values))
            for type_key, constants in cons.items()
            for constant_key, values in constants.items()]


def generate_py_constants(path_to_constants, class_output, localized_output):
    """
    Generates Python classes using a constants.yaml file, a pot file and
    po files with translations to be used with Babel

    Parameters
    ----------
    path_to_constants: str
        Folder where the constants.yaml file is located, locales should be at
        path_to_constants/locales/{locale}.yaml
    class_output: str
        Where to store the constants.py file
    localized_output: str
        Where to store the messages.pot file and the messages.po
        files

    Notes
    -----
    Assumes a flask-babel folder-like structure, constants_messages.pot
    will be stored in localized_output/constants_messages.pot and po files in
    localized_outout/constants_translations/{locale}/LC_MESSAGES/messages.po

    Once you hace the po files, you can compile them to mo files using the
    babel command line tools (you may need to use the -f option if babel
    says you files are 'fuzzy')

    If you are already using Babel for translation you need to make sure you
    load all folders with translations, in flask-babel you can configure this
    in the flask config file by doing:
    BABEL_TRANSLATION_DIRECTORIES  = 'directoryA;directoryB'
    """
    env = Environment(
        loader=PackageLoader('krieger_constants', 'templates'),
        # what is this?
        autoescape=select_autoescape(['html', 'xml'])
    )

    path_to_constants_file = os.path.join(path_to_constants, 'constants.yaml')

    classes = _process_constants(path_to_constants_file)
    path_to_locales = os.path.join(path_to_constants, 'locales')
    locale_files = [f for f in os.listdir(path_to_locales)]

    # save constants.py
    template_classes = env.get_template('constants.py.template')
    content_classes = template_classes.render(classes=classes)

    with open(os.path.join(class_output, 'constants.py'), 'w') as f:
        f.write(content_classes)

    # append to messages.pot
    template_pot = env.get_template('messages.pot.template')
    content_pot = template_pot.render(classes=classes)

    path_to_pot = os.path.join(localized_output, 'constants_messages.pot')

    with open(path_to_pot, 'w') as f:
        f.write(content_pot)

    template_po = env.get_template('messages.po.template')

    for locale_file in locale_files:
        path_to_locale = os.path.join(path_to_locales, locale_file)
        locales = _process_locale(path_to_constants_file, path_to_locale)
        content_po = template_po.render(locales=locales)
        path_to_po = os.path.join(localized_output, 'constants_translations',
                                  locale_file.replace('.yaml', ''),
                                  'LC_MESSAGES')

        if not os.path.exists(path_to_po):
            os.makedirs(path_to_po)

        with open(os.path.join(path_to_po, 'messages.po'), 'w') as f:
            f.write(content_po.encode('UTF-8'))


def generate_js_constants(path_to_constants, class_output, localized_output):
    """
    Generates Javascript objects using a constants.yaml file and json files
    with translations to be used with Babel

    Parameters
    ----------
    path_to_constants: str
        Folder where the constants.yaml file is located
    class_output: str
        Where to store the constants.js file
    localized_output: str
        Where to store the {locale}.json files
    """
    env = Environment(
        loader=PackageLoader('krieger_constants', 'templates'),
        # what is this?
        autoescape=select_autoescape(['html', 'xml'])
    )

    path_to_constants_file = os.path.join(path_to_constants, 'constants.yaml')

    classes = _process_constants(path_to_constants_file)
    path_to_locales = os.path.join(path_to_constants, 'locales')
    locale_files = [f for f in os.listdir(path_to_locales)]

    # save constants.py
    template_classes = env.get_template('constants.js.template')
    content_classes = template_classes.render(classes=classes)

    with open(os.path.join(class_output, 'constants.js'), 'w') as f:
        f.write(content_classes)

    # append to every {locale}.json file
    for locale_file in locale_files:
        path_to_locale = os.path.join(path_to_locales, locale_file)
        locales = _process_locale(path_to_constants_file, path_to_locale)

        # get id to translation dicts
        id2translation = [cls['constants'] for cls in locales]

        # flatten list
        id2translation = [item for sublist in id2translation
                          for item in sublist]

        # convert to id: translation
        id2translation = {d['id']: d['translation'] for d in id2translation}

        # store as json
        path_to_locale = os.path.join(localized_output,
                                      locale_file.replace('yaml', 'json'))

        with open(path_to_locale, 'w') as f:
            json.dump(id2translation, f)
