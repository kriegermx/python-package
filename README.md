# Krieger-gae

## Install

```
pip install git+https://bitbucket.org/kriegermx/python-package
```

Or add the following line to your `requirements.txt` file:

```
git+https://bitbucket.org/kriegermx/python-package
```

## Docs

Online docs for the latest version are [here](https://krieger-gae.aerobatic.io/). (password: krieger)

### Build

```
cd docs
make html
```

### Deploy

```
aero deploy
```

To change password edit the `aerobatic.yml` file.