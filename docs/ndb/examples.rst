Examples
========

This sections shows two examples, both are based on the following models,
permissions and guard declarations.

This is how your `models.py` file would look like:

.. literalinclude:: ../../examples/models.py

`permissions.py`:

.. literalinclude:: ../../examples/permissions.py

`guard.py`

.. literalinclude:: ../../examples/guard.py

Basic example
-------------

Assuming you can import the previous three files, you can perform basic
operations on your models like this:

.. literalinclude:: ../../examples/basic.py


Flask example
-------------

This is how a complete CRUD flask controller would look like:

.. literalinclude:: ../../examples/flask.py