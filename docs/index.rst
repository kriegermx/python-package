.. krieger-gae documentation master file, created by
   sphinx-quickstart on Thu Nov 10 23:26:23 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to krieger-gae's documentation!
***************************************

Installation
============

Using `pip`:

.. code-block:: shell

    pip install git+https://bitbucket.org/kriegermx/python-package

Or add the following line to your `requirements.txt` file:

.. code-block:: shell

    git+https://bitbucket.org/kriegermx/python-package


Contents
========

.. toctree::
   :maxdepth: 2

   ndb
   exceptions
   notifier
   api
   util
   testing
   token
   constants


.. include:: ../CHANGELOG.rst


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

