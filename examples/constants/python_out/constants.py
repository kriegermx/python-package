from krieger_gae.util import Enum


class IncidentStatus(Enum):
    Waiting = 'waiting'
    Accepted = 'accepted'
    Rejected = 'rejected'


class IncidentType(Enum):
    Medical = 'medical'
    Car = 'car'
    Individual = 'individual'

