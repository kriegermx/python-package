# -*- coding: utf-8 -*-
from copy import copy
import logging
from functools import wraps
import collections
from datetime import datetime, date
import json
from functools import partial

from ..exceptions import BadRequest, NotFound, Forbidden
from .introspection import DatastoreModelIntrospector
from .helpers import (_is_collection, _unwrap_mixed_iterator,
                      _is_key_or_iterable_of_keys,
                      _map_parameters_in_fn_call, _wrap_in_list)
from .output import prettify_iterator
from .parsers import json_loads, to_datetime, to_date, to_blobkey

from google.appengine.ext.ndb import (Key, IntegerProperty, FloatProperty,
                                      KeyProperty, BooleanProperty,
                                      DateTimeProperty, DateProperty,
                                      StringProperty, StructuredProperty,
                                      BlobKeyProperty, LocalStructuredProperty)
from google.appengine.ext import ndb
from google.appengine.ext.blobstore import blobstore

logger = logging.getLogger(__name__)

"""
This is one of the most important modules in the project, it handles all
user input from validating parameterst to parsing keys.

Note that most methods here raise custom exceptions
(e.g. BadRequest, NotFound), since this module is used in the guards
mostly in the form of decorators, we want errors here to be handled by
endpoints and flask gracefully, hence, exceptions raised here contain
appropriate information to be displayed as error messages.
"""

#####################
# UTILITY FUNCTIONS #
#####################


def datetime2str(dt):
    """
    Parse a datetime object and convert it to a string if object evaluates
    to false return None
    """
    return datetime.strftime(dt, "%Y-%m-%d %H:%M:%S") if dt else None


def date2str(dt):
    """
    Parse a date object and convert it to a string if object evaluates
    to false return None
    """
    return datetime.strftime(dt, "%Y-%m-%d") if dt else None


def json2structured(data, property_obj):
    """
        Casts a json string or mapping to the corresponding
        Structured property
    """
    # https://cloud.google.com/appengine/docs/python/ndb/modelclass
    cls_name = property_obj.__dict__['_modelclass'].__name__
    cls = ndb.Model._lookup_model(cls_name)
    return cls(**_parse_properties(data, cls))


def _applyfn(obj, fn, *args, **kwargs):
    """
        Applies a function to obj or applies a fn to every
        element in obj if obj is a collection
    """
    if _is_collection(obj):
        return [fn(o, *args, **kwargs) for o in obj]
    else:
        return fn(obj, *args, **kwargs)


def cast_value_to_ndb_property(value, property_obj, fail_silently=True):
    """Function for parsing: DateTimeProperty (from iso unicode string),
    DateProperty (from iso unicode string), BlobKeyProperty
    (from unicode string), StructuredProperty (from dict),
    LocalStructuredProperty (from dict), IntegerProperty (parsed using int),
    FloatProperty (parsed using float), StringProperty (parsed using unicode),
    KeyProperty (from urlsafe string)

    Parameters
    ----------
    value: str or dict
        Value to be parsed
    property_obj: ndb property
        Property object from a ndb.Model object

    Raises
    ------
    NotImplementedError
        If property type is unsupported

    Notes
    -----
    The function does not modify the value if can't be parsed
    """

    # TODO: cast keys
    # Mapping from ndb model property type to casting function
    property2casting_fn = {
        DateTimeProperty: to_datetime,
        DateProperty: to_date,
        BlobKeyProperty: to_blobkey,
        StructuredProperty: json2structured,
        LocalStructuredProperty: json2structured,
        IntegerProperty: int,
        FloatProperty: float,
        StringProperty: unicode,
        KeyProperty: lambda s: ndb.Key(urlsafe=s)
    }

    # Mapping from ndb property type from python data type we can parse from
    property2python = {
        DateTimeProperty: [unicode, str],
        DateProperty: [unicode, str],
        BlobKeyProperty: [unicode, str],
        StructuredProperty: [collections.Mapping],
        LocalStructuredProperty: [collections.Mapping],
        IntegerProperty: [unicode, str, int],
        FloatProperty: [unicode, str, long, float],
        StringProperty: [unicode, str, long, int, float],
        KeyProperty: [unicode, str],
    }

    property_type = type(property_obj)
    repeated = True if property_obj.__dict__.get('_repeated') else False

    # Raise error if unsupported type
    if property_type not in property2casting_fn.keys():
        raise NotImplementedError("Cannot parse property of type {}"
                                  .format(property_type.__name__))

    casting_fn = property2casting_fn[property_type]

    if (isinstance(property_obj, StructuredProperty) or
       isinstance(property_obj, LocalStructuredProperty)):
        casting_fn = partial(casting_fn, property_obj=property_obj)

    def cast(value):
        if any([isinstance(value, cls) for cls
                in property2python[property_type]]):
            return casting_fn(value)
        else:
            logger.debug("Cannot parse property with type {} from {} type "
                         "ignoring value".format(property_type, type(value)))
            return value

    if repeated:
        res = [cast(v) for v in value]
    else:
        res = cast(value)

    return res

######################################################################
# INPUT VALIDATION WHEN SUBMITTING FORMS TO CREATE OR UPDATE OBJECTS #
######################################################################


def _validate_keys_in_input_form(data, cls, check_required):
    """
     Validates a mapping object to a ndb model. It checks that every
     key in data appears in the model and optionally that every
     required property is present.

     If cls constains StructuredProperties it also validates it
    """
    if not isinstance(data, collections.Mapping):
        raise ValueError(("data must be a mapping object, got {}"
                          .format(type(data))))

    def is_required(prop):
        try:
            return prop.__dict__['_required']
        except KeyError:
            return False

    properties = cls.__dict__['_properties']
    properties_names = set(properties.keys())
    args_names = set(data.keys())

    # Check that all keys in args appear as properties in the model
    if not args_names.issubset(properties_names):
        # Return a list of extra arguments that are not properties
        extra = prettify_iterator(args_names - properties_names)
        message = 'Invalid fields {} for {}'.format(extra, cls.__name__)
        user_message = 'Hubo un error en los datos de entrada'
        raise BadRequest(message, user_message)

    # Check that all required properties are present
    required = set([p for p, v in properties.items() if is_required(v)])

    # Check that all required properties are present
    if (not required.issubset(args_names) and check_required):
        missing_required = prettify_iterator(required - args_names)
        raise BadRequest('util.input.missing_req'.format(missing_required))

    # Check if there are complex properties such as StructuredProperty
    complex_props = {k: v for k, v in properties.items()
                     if isinstance(v, ndb.StructuredProperty)}

    # Validate complex_props if any, but only if they are collections.Mapping
    # objects since they still need validatio. (e.g. a StructuredProperty
    # of type Phone still nees to be validated if data['phone'] is a
    # dictionary but we can skip the validation if data['phone'] is already
    # a Phone instance)
    for key, value in complex_props.items():
        if isinstance(data.get(key), collections.Mapping):
            _validate_keys_in_input_form(data[key],
                                         value.__dict__['_modelclass'],
                                         check_required)


def _parse_properties(data, cls):
    """
    Tries to cast a data dictionary, matching against a datastore model class

    Parameters
    ----------
    data: dict
        Input daa
    cls: ndb.Model
        Datastore model

    Returns
    -------
    dict
        A dictionary with keys with '' and None values removed and parsed
        properties, see cast_value_to_ndb_property to see which properties
        can be parsed

    Notes
    -----
    If the property type casting is unsupported, the value is ignored
    """
    properties = cls._properties

    # Remove keys with empty strings or None
    clean = {k: v for k, v in data.items() if v != '' and v is not None}

    # Check that every key in data appears as property in the datastore model
    if not set(data.keys()) <= set(properties.keys()):
        raise ValueError('Cannot parse data to a {} instance, properties are: '
                         '{} but data contains {}'.format(cls.__name__,
                                                          properties.keys(),
                                                          data.keys()))

    supported = (DateTimeProperty, DateProperty, BlobKeyProperty,
                 StructuredProperty, LocalStructuredProperty, IntegerProperty,
                 FloatProperty, StringProperty, KeyProperty)

    def cast(v, prop):
        if any([isinstance(prop, cls) for cls in supported]):
            return cast_value_to_ndb_property(v, prop)
        else:
            return v

    # Cast properties if necessary
    clean = {k: cast(v, properties[k]) for k, v in clean.items()}

    return clean


def validate_input_form(args, cls, check_required=True, parse=True):
    """
    Validates an input form (dictionary) according to an ndb_model,
    it checks that every key in args is a property in the model and raises
    appropriate errors. It can optionally check if all required properties are
    present and parse some properties (see cast_value_to_ndb_property function)

    Raises
    ------
    ValueError
        If args is not a mapping object

    Returns
    -------
    dict
        A dictionary with parsed properties if parse=True otherwise returns
        args

    """
    # TODO: improve error management

    if not isinstance(args, collections.Mapping):
        raise ValueError('args must be a collections.Mapping object')

    # First validate keys in the dictionary to make sure all keys in the dict
    # are valid properties and optionally check that all required properties
    # appear
    _validate_keys_in_input_form(args, cls, check_required)

    if parse:
        args = _parse_properties(args, cls)

    return args


def validate_generic_form(form, requires, optional=None):
    """Function to validate generic formss

    Most of the time, the input validation is done against a NDB model, but
    sometimes we need to validate othe types of input (e.g. when reseting
    a password we only expect a password key to exist) use this function
    to validate such forms
    """
    if not optional:
        optional = set()

    includes = set(form.keys())
    requires = set(requires)

    # Check if there are missing arguments
    missing = requires - includes

    if missing:
        logger.error('Failed to validate form: {} Required: {} Optional {}. '
                     'Missing required parameters: {}'
                     .format(form, requires, optional, missing))
        raise BadRequest('lib.input.missing_params'
                       .format(prettify_iterator(missing)))

    # Check if there are extra arguments
    extra = includes - requires - optional

    if extra:
        logger.error('Failed to validate form: {} Required: {} Optional {}. '
                     'Got extra parameters: {}'
                     .format(form, requires, optional, extra))
        raise BadRequest('lib.input.extra_params'.format(prettify_iterator(extra)))


#########################################################
# UTILITIES FOR PARSING PARAMETERS WHEN LISTING OBJECTS #
#########################################################

def list_with_request_args(args, guard, **kwargs):
    """Proxy function to call guard.list using a dictionary with values from
    a request

    When using Flask or endpoints you may need to sort order, and filter list
    results and the input will be provided in a dict-like object, this function
    parses such object and converts the contents to valid calling parameters
    for the guard.list function

    Examples
    --------
    .. code-block:: python

        # Getting only one element
        list_with_request_args(dict(count=1), self.guard)

        # Getting only one element with offset
        list_with_request_args(dict(count=1, offset=2), self.guard)

        # Using cursors
        args = dict(get_cursor=True, count=1)
        res, cursor, more = list_with_request_args(args, self.guard)

        # Filtering
        args = dict(email='dude0@krieger.mx')
        list_with_request_args(args ,self.guard)

        # Filtering with more than one value
        args = dict(email=['dude0@krieger.mx', 'dude1@krieger.mx'])
        list_with_request_args(args ,self.guard)

        # Ordering
        args = dict(order='-email')
        list_with_request_args(args, self.guard)

        # Ordering with more than one value
        args = dict(order=['-email', 'role'])
        list_with_request_args(args, self.guard)

    Notes
    -----
    You can directly pass a ImmutableMultiDict as returned from flask
    request.args
    """
    def _has_content(o):
        try:
            return len(o) > 0
        except TypeError:
            return True

    def unwrap(v):
        return v if len(v) > 1 else v[0]

    try:
        args = {k: unwrap(v) for k, v in args.iterlists()}
    except AttributeError:
        pass

    # Remove None values and empty collections
    args = {k: v for k, v in args.items() if v is not None and _has_content(v)}

    # Validate count argument
    if 'count' in args.keys() and args['count'] <= 0:
        raise BadRequest('util.parsing_list_args.count_greater_than_zero')

    # Validate cursor argument
    if 'offset' in args.keys() and args['offset'] < 0:
        raise BadRequest('util.parsing_list_args.cursor_greater_than_zero')

    if 'query' in args.keys():
        logger.debug('query in params, performing search...')
        return query_entity(args['query'], guard)
    else:
        params = parse_url_params(args, guard._model)
        params.update(kwargs)

        logger.debug('Calling guard.list with params: {}'.format(params))
        return guard.list(**params)


def query_entity(query, guard):
    """
    Queries an entity from a querystring (q=<str>)
    """
    # TODO: check the length of the query string

    # Build an OR clause with every indexed property
    # TODO: remove this ._model
    cls = guard._model
    introspector = DatastoreModelIntrospector(cls)
    indexed = introspector.indexed_properties()

    logger.debug('Called query_entity. Indexed props for {} are {}'
                 .format(cls, indexed))

    def make_node(prop):
        try:
            casted_query = cast_value_to_ndb_property(query, prop)
            return (getattr(cls, prop) >= casted_query, prop, casted_query)
        except KeyError as e:
            logger.debug("Couldn't cast, returning None. KeyError: {}"
                         .format(e))
            return None
        except ValueError as e:
            logger.debug("Couldn't cast, returning None. ValueError: {}"
                         .format(e))
            return None

    nodes = [make_node(indexed_prop) for indexed_prop in indexed]
    logger.debug('Resulting nodes are: {}'.format(nodes))
    nodes = [node for node in nodes if node is not None]
    logger.debug('Non-empty nodes are: {}'.format(nodes))

    # use query method in the user to perform the search
    res = []
    for node, prop, query in nodes:
        partial = guard.list(filter_=(node,))
        # remove entities that don't start with the criteria (str)
        # or whose value is not equal to query (numbers)
        if isinstance(query, basestring):
            partial = [r for r in partial if
                       getattr(r, prop).startswith(query)]
        else:
            partial = [r for r in partial if getattr(r, prop) == query]

        res.extend(partial)
    return res


def parse_url_params(params, cls):
    '''
        Parse a dictionary with URL parameters, the following are
        possible values:

        - count (number of elements to return)
        - cursor (numper of elements to skip)
        - order (one or more elements used for ordering)

        If the params object contains more keys they will be used
        to filter the results eg users?name=stuff

        Returns a new dictionary with the following

        - count (returns the value found or a default if not present)
        - cursor (same as count)
        - order (returns an order object using cls and every element
                in the key, values can start with '-' to change the order)
        - filter (returns a filter object)

        Raise a BadRequest exception if the input is malformed
    '''
    # TODO: check if this function is still needed
    # params = unwrap_url_params(params)

    logger.debug('Parsing params {}'.format(params))

    parsed = copy(params)

    if 'order' in params:

        # _order_from_list expects an iterable, in case you only got one value
        # convert it
        order_params = _wrap_in_list(params['order'])

        try:
            parsed['order'] = _order_from_list(order_params, cls)
        except AttributeError:
            logger.debug('Error parsing order elements')
            raise BadRequest("Couldn't parse order parameters, check that "
                             "every parameter sent is in the model and "
                             "indexed")

    # Get the remaining parameters, they must be for filtering
    params_ = set(params)
    # Get the keys that are going to be used for filtering
    filter_keys = params_ - set(('count', 'cursor', 'order', 'q', 'offset',
                                 'get_cursor', 'output'))

    # Build a filter sub dictionary
    if filter_keys:

        # Remove keys
        for key in filter_keys:
            parsed.pop(key)

        try:
            filter_sub = {k: v for k, v in params.items() if k in filter_keys}
            parsed['filter'] = _filter_from_query_params(filter_sub, cls)
        except AttributeError as e:
            logger.debug('Error parsing filter elements: {}'.format(e))
            raise BadRequest

    logger.debug('Parsed params: {}'.format(parsed))

    return parsed


def _unwrap_url_params(params, cast=int):
    #     str2operator = {
    #     '<': operator.lt,
    #     '<=': operator.le,
    #     '=': operator.eq,
    #     '!=': operator.ne,
    #     '>=': operator.ge,
    #     '>': operator.gt,
    # }
    logger.debug('Unwrapping url params: {}'.format(params))

    params = dict(params)

    for k, v in params.items():
        params[k] = params[k][0]

    def cast_if_possible(v):
        try:
            return cast(v)
        except:
            return v

    def to_list_if_commas(v):
        if ',' in v:
            return v.split(',')
        else:
            return v

    def process(v):
        return cast_if_possible(to_list_if_commas(v))

    res = {k: process(v) for k, v in params.items()}

    # If order only has one element, convert to a list
    if ('order' in res) and (not isinstance(res['order'], list)):
        res['order'] = [res['order']]

    logger.debug('Unwrapped url params: {}'.format(res))
    return res


def _filter_from_query_params(params, cls):
    filter_ = []

    # Parse urlsafe keys
    introspector = DatastoreModelIntrospector(cls)
    params = introspector.parse_urlsafe_from_dict(params)

    # for every parameter build a Entity.Key == Value
    # expression and concatenate them
    for key, val in params.items():
        if _is_collection(val):
            # Build an IN node for every value in the list
            entity_attr = getattr(cls, key)
            or_node = getattr(entity_attr, 'IN')(val)
            filter_.append(or_node)
        else:
            # Not a list, just create one node
            element = getattr(cls, key) == val
            filter_.append(element)

    return filter_


def _order_from_list(elements, cls):
    def build_order_node(v):
        if v.startswith('-'):
            v = v.replace('-', '')
            return -getattr(cls, v)
        else:
            return getattr(cls, v)

    return [build_order_node(v) for v in elements]


#######################################
# DECORATORS FOR PARSING URLSAFE KEYS #
#######################################


def parse_urlsafe_keys(cls):
    """
    When this decorator is applied to an instance method it parses
    any urlsafe Keys and converts them to Keys, takes a datastore model
    as a reference to know which properties are Keys
    """
    def parse_urlsafe_keys_with_class(func):
        @wraps(func)
        def func_wrapper(self, *args, **kwargs):
            kwargs = _parse_keys_in_dictionary_with_model(kwargs, cls)
            return func(self, *args, **kwargs)

        return func_wrapper

    return parse_urlsafe_keys_with_class


def parse_urlsafe_keys_in_kwargs_with_model(func):
    """
    When this decorator is applied to an instance method it parses
    any urlsafe Keys in the kwargs and converts them to Keys, uses self._model
    as a reference to know which properties are Keys
    """
    @wraps(func)
    def func_wrapper(self, *args, **kwargs):
        kwargs = _parse_keys_in_dictionary_with_model(kwargs, self._model)
        return func(self, *args, **kwargs)

    return func_wrapper


def _parse_keys_in_dictionary_with_model(data, model):
    """
    Takes a dictionary and a datastore model and parses any urlsafe
    Key (including BlobKeys) and converts them to a Key (or BlobKey) object

    Parameters
    ----------
    data: dict
        A dictionary with (property: value) pairs to match agains the ndb.Model
    model: ndb.Model
        A ndb.Model to be used as reference

    Returns
    -------
    dict
        A dictionary with the urlsafe keys replace with Key and BlobKey objects

    Raises
    ------
    NotFound
        If failed to parse a urlsafe Key
    """
    # Find key properties for the model
    intro = DatastoreModelIntrospector(model)
    data_props = set(data.keys())
    key_props = intro.key_properties()
    blob_props = intro.blob_key_properties()

    # Find which key properties appear in kwatds, those are the ones
    # that need processing
    keys_to_replace = key_props.intersection(data_props)
    blobs_to_replace = blob_props.intersection(data_props)

    for name in keys_to_replace:
        # Get current value, this should be either a urlsafe key,
        # an iterator of urlsafe keys, a Key or an interator of Keys
        value = data[name]

        # Parse urlsafe key or iterator of urlsafe keys
        if _is_collection(value):
            data[name] = [_convert_to_keys(v) for v in value]
        else:
            data[name] = _convert_to_keys(value)

    for name in blobs_to_replace:
        value = data[name]

        if _is_collection(value):
            data[name] = [blobstore.BlobKey(v) for v in value]
        else:
            data[name] = blobstore.BlobKey(value)

    return data


def parse_and_validate_urlsafe_keys_in_args(params, cls):
    """
    This decorator receives a parameter or an iterable of parameters that are
    urlsafe Keys and converts them to Key objects, it also checks that keys
    are of kind cls.
    """
    def parse_and_validate_urlsafe_keys(func):
        @wraps(func)
        def func_wrapper(self, *args, **kwargs):
            # Many params parsed to the same Key class
            if _is_collection(params) and not _is_collection(cls):
                for param in params:
                    kwargs = _parse_and_validate_urlsafekeys_in(cls,
                                                                args,
                                                                kwargs,
                                                                func,
                                                                param)
            # Many params parsed to many Key classes
            elif _is_collection(params) and _is_collection(cls):
                for param, a_cls in zip(params, cls):
                    kwargs = _parse_and_validate_urlsafekeys_in(a_cls,
                                                                args,
                                                                kwargs,
                                                                func,
                                                                param)

            # One param parsed to one key class
            elif not _is_collection(params) and not _is_collection(cls):
                kwargs = _parse_and_validate_urlsafekeys_in(cls,
                                                            args,
                                                            kwargs,
                                                            func,
                                                            params)
            else:
                raise ValueError("Can't parse keys, check your argument types")

            return func(self, **kwargs)

        return func_wrapper

    return parse_and_validate_urlsafe_keys


def parse_and_validate_urlsafe_keys_in_args_with_model(params):
    """
    This decorator receives a parameter or an iterable of parameters that are
    urlsafe Keys and converts them to Key objects, it also checks that keys
    are of kind cls by looking at the self._model property
    """
    def parse_and_validate_urlsafe_keys(func):
        @wraps(func)
        def func_wrapper(self, *args, **kwargs):
            if _is_collection(params):
                for param in params:
                    kwargs = _parse_and_validate_urlsafekeys_in(self._model,
                                                                args,
                                                                kwargs,
                                                                func,
                                                                param)
            else:
                kwargs = _parse_and_validate_urlsafekeys_in(self._model,
                                                            args,
                                                            kwargs,
                                                            func,
                                                            params)
            return func(self, **kwargs)
        return func_wrapper
    return parse_and_validate_urlsafe_keys


def _convert_to_keys(key, fail_silently=False):
    """
    Receives a key Key (or iterable of keys) and converts them
    to Key objects. If key is already a Key object, the function
    just returns the value.
    """
    def _safe_convert(k):
        if isinstance(k, Key):
            return k
        elif isinstance(k, basestring):
            try:
                return Key(urlsafe=k)
            except Exception as e:
                msg = "Can't convert {} ({}) to Key: {}".format(type(k), k, e)
                if fail_silently:
                    logger.debug('Failing silently: {}'.format(msg))
                    return None
                else:
                    logger.debug('Not found: {}'.format(msg))
                    raise NotFound(msg)
        else:
            msg = ('key is not a Key neither a urlsafe Key, type: {}. '
                   'Check that you included all parameters in the function '
                   'call'
                   .format(type(k)))
            if fail_silently:
                logger.debug('Failing silently: {}'.format(msg))
                return None
            else:
                logger.debug('Not found: {}'.format(msg))
                raise NotFound(msg)

    if _is_collection(key):
        for k in key:
            return [_safe_convert(k) for k in key]
    else:
        return _safe_convert(key)


def _ensure_key_matches_kind(key, kind):
    """
    Receives a Key or iterable of Keys and checks if the Key (or every Key)
    matches the given kind

    Notes
    -----
    In case you have polymodels, the comparison will be made against the
    root class
    """
    if _is_collection(key):
        for k in key:
            matches = all([k.kind() == kind for k in key])
            if not matches:
                msg = ('Expected key of kind {} but not all '
                       'keys had that kind'.format(kind))
                logger.debug(msg)
                raise NotFound(msg)
    else:
        if key.kind() != kind:
            msg = ('Expected key of kind {} but got {} instead'
                   .format(kind, key.kind()))
            logger.debug(msg)
            raise NotFound(msg)


def _parse_and_validate_urlsafekeys_in(cls, args, kwargs, func, name):
    """
    Helper function to parse and validate, parses urlsafe Keys and converts
    them to Keys, also checks that the Keys are the right kind

    Parameters
    ----------

    cls: Class or str
        Class used to validate Keys
    """
    logger.debug('Parsing and validating. cls: {} args: {} kwargs: {} fn: {} '
                 'name: {}'.format(cls, args, kwargs, func, name))

    parsed_params = _map_parameters_in_fn_call(args, kwargs, func)

    try:
        # Parse key and modify it
        raw_param = parsed_params[name]
        parsed_params[name] = _convert_to_keys(raw_param)
    except KeyError:
        msg = "Couldn't locate {} in the function call"
        logger.debug(msg)
        raise NotFound(msg.format(name))

    # Check that param has the correct class but only if a class was declared
    # otherwise skip validation
    if cls:
        # cls can be a string in which case you should use it directly
        if isinstance(cls, basestring):
            class_name = cls
        else:
            # https://cloud.google.com/appengine/docs/standard/python/ndb/polymodelclass
            class_name = cls._get_kind()

        _ensure_key_matches_kind(key=parsed_params[name], kind=class_name)

    logger.debug('Parsing results. {}'.format(parsed_params))
    return parsed_params


################################
# VALIDATING NAMESPACE IN KEYS #
################################


def _check_key_namespace(namespace, obj):
    """
    Validate a Key object against a namespace

    Parameters
    ----------
    namespace: str
        Namespace name
    obj: ndb.Key
        A key to be compared


    Raises
    ------
    Forbidden
        If the key does not match namespace

    Notes
    -----
    No exception is raised if obj.namespace() evaluates to False
    """
    if obj.namespace() != namespace and obj.namespace():
        logger.info('Forbidden: attempting to perform operation on'
                    ' namespace {}, but guard is on namespace {}'
                    .format(obj.namespace(), namespace))
        raise Forbidden(("Cannot perform operation on object from a "
                                "different namespace: Guard is in namespace {}"
                                " and object is in namespace {}"
                                .format(namespace, obj.namespace())))


def validate_namespace_in_keys(func):
    """
    This decorator checks that every Key in args and kwargs has the same
    namespace as the guard trying to perform an operation on it. The only
    exception is when param exists in the Empty namespace, in that case, no
    exception is raised

    The method only works when the parameters passed are Keys (not urlsafe
    keys) or when the parameters can be iterated, in which case it looks at
    every element. In case you need to check urlsafe keys, parse them first
    and them applied this decorator.
    """
    # TODO: remove casting from here to make if only work with keys and
    # not urlsafe keys
    @wraps(func)
    def func_wrapper(self, *args, **kwargs):

        # Get every value passed as args or kwarg
        all_args = copy(list(args))
        all_args.extend(kwargs.values())
        # Since keys may be hidden in iterators unlist the arguments
        all_args = _unwrap_mixed_iterator(all_args)

        # # Try to convert everything to a key
        # only_keys = [arg for arg in all_args
        #              if _is_key_or_iterable_of_keys(arg)]

        # Try to convert everything to a key
        only_keys = [_convert_to_keys(arg, fail_silently=True) for arg in
                     all_args if
                     isinstance(arg, Key) or isinstance(arg, basestring)]

        # Remove unsuccessful casts
        successful_casts = [key for key in only_keys if key]

        for arg in successful_casts:
            _check_key_namespace(self._namespace, arg)

        return func(self, *args, **kwargs)
    return func_wrapper
