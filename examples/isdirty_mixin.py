from krieger_gae.ndb.mixins import IsDirtyMixin
from google.appengine.ext import ndb


class DummyModel(IsDirtyMixin, ndb.Model):
    name = ndb.StringProperty()
    email = ndb.StringProperty()


user = DummyModel(name='name')
user.name = 'new name'
user.email = 'newemail@stuff.com'

user.is_dirty('name')  # True
user.dirty_properties()  # {'name', 'email'}

user.put()

user.is_dirty('name')  # False
