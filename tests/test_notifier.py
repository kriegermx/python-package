from krieger_gae import Notifier
from google.appengine.ext import ndb
from jinja2 import FileSystemLoader
from krieger_gae.testing import MultiDatastoreTestCase

from .ndb.dummy import User


class Notification(ndb.Model):
    assigned = ndb.KeyProperty('User', required=True)
    title = ndb.StringProperty()
    content = ndb.StringProperty()
    resource = ndb.KeyProperty()
    extra = ndb.JsonProperty()
    date = ndb.DateTimeProperty(auto_now_add=True)

    def to_dict(self):
        d = super(Notification, self).to_dict()
        d['key'] = self.key.urlsafe()
        return d


class TestNotifier(MultiDatastoreTestCase):

    def setUp(self):
        super(TestNotifier, self).setUp()

        self.email = 'user@krieger.mx'
        user = User(email=self.email)
        self.user_key = user.put().urlsafe()

        self.notifier = Notifier(Notification,
                                 email_master='master@krieger.mx',
                                 loader=FileSystemLoader('tests/templates'))

    def test_can_load_template(self):
        assert self.notifier.env.get_template('new_user.html')

    def test_can_save_notification_to_datastore(self):
        self.notifier.send(recipient=self.user_key,
                           title='title',
                           content='content')

        notifs = Notification.query().fetch()
        notif = notifs[0]
        assert (len(notifs) == 1 and notif.title == u'title' and
                notif.content == u'content')

    def test_can_send_notification_with_resource(self):
        self.notifier.send(recipient=self.user_key,
                           title='title',
                           content='content',
                           resource=self.user_key)

        notifs = Notification.query().fetch()
        notif = notifs[0]
        assert (len(notifs) == 1 and notif.title == u'title' and
                notif.content == u'content' and
                notif.resource == ndb.Key(urlsafe=self.user_key))

    def test_can_render_template_with_extra(self):
        self.notifier.send(recipient=self.user_key,
                           title='sup {{name}}',
                           content='this is the content: {{message}}',
                           extra=dict(name='Pedro', message='sup dude'))
        notifs = Notification.query().fetch()
        notif = notifs[0]
        assert (len(notifs) == 1 and notif.title == u'sup Pedro' and
                notif.content == u'this is the content: sup dude')

    def test_can_save_notif_and_send_email(self):
        self.notifier.send(recipient=self.user_key,
                           title='title',
                           content='super duper content',
                           email_template='generic.html')

        messages = self.mail_stub.get_sent_messages(to='user@krieger.mx')
        msg = messages[0]
        assert (len(messages) == 1 and msg.subject == u'title' and
                u'super duper content' in msg.html.payload)

    def test_can_defer_notification(self):
        self.notifier.send(recipient=self.user_key,
                           title='title',
                           content='super duper content',
                           email_template='generic.html',
                           send_now=False)

        tasks = self.taskqueue_stub.get_filtered_tasks()
        assert len(tasks) == 1

    def test_can_defer_multiple_notifications(self):
        for i in range(100):
            self.notifier.send(recipient=self.user_key,
                               title='title',
                               content='super duper content',
                               email_template='generic.html',
                               send_now=False)

        tasks = self.taskqueue_stub.get_filtered_tasks()
        assert len(tasks) == 100

    def test_can_render_content_in_content_email_tag(self):
        self.notifier.send(recipient=self.user_key,
                           title='title',
                           content='super duper content',
                           email_template='generic.html')

        messages = self.mail_stub.get_sent_messages(to=self.email)
        payload = ('This is a generic notification with content:\n\n'
                   'super duper content\n\nSee more at: ')

        assert len(messages) == 1 and messages[0].html.payload == payload

    def etst_can_include_extra_content_for_email_rendering(self):
        self.notifier.send(recipient=self.user_key,
                           title='title',
                           content='super duper content',
                           email_template='generic.html',
                           extra_email=dict(url='http://krieger.mx'))

        messages = self.mail_stub.get_sent_messages(to=self.email)
        payload = ('This is a generic notification with content:\n\n'
                   'super duper content\n\nSee more at: http://krieger.mx')

        assert len(messages) == 1 and messages[0].html.payload == payload
