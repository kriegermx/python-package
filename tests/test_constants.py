from krieger_constants.generate_constants import _process_constants
import unittest
import json


def dict_compare(d1, d2):
    """Compare two dictionaries

    Notes
    -----
    http://stackoverflow.com/questions/4527942/comparing-two-dictionaries-in-python
    """
    d1_keys = set(d1.keys())
    d2_keys = set(d2.keys())
    intersect_keys = d1_keys.intersection(d2_keys)
    same = set(o for o in intersect_keys if d1[o] == d2[o])
    return same == intersect_keys == d1_keys == d2_keys


class TestConstants(unittest.TestCase):

    def test_process_constants(self):
        constants = _process_constants('tests/files/constants.yaml')

        with open('tests/files/expected_process_constants.json') as f:
            expected = json.load(f)

        assert all([dict_compare(d1, d2) for d1, d2 in zip(constants,
                    expected)])
