import unittest

from .dummy import User, MyUserGuard
import krieger_gae
from krieger_gae.exceptions import BadRequest, InternalServerError
from krieger_gae.ndb.constants import UserStatus
from krieger_gae.testing import SingleDatastoreTestCase
from krieger_gae.token import TokenType, generate

krieger_gae.set_token_secret('my-token-secret')


class UserGuardTesting(SingleDatastoreTestCase):

    def setUp(self):
        super(UserGuardTesting, self).setUpClass()

        user = User(email='hola@krieger.mx', status=UserStatus.Active)
        user.put()

        other = User(email='master@krieger.mx')
        self.other = other.put().urlsafe()

        self.guard = MyUserGuard(user)

    def test_can_bootstrap_account(self):
        assert MyUserGuard.bootstrap_account(password='mypassword',
                                             email='myemail@krieger.mx')

    def test_can_create_account(self):
        assert self.guard.create_account(email='dude@krieger.mx')

    def test_cannot_create_account_if_user_already_exists(self):
        with self.assertRaises(BadRequest):
            self.guard.create_account(email='hola@krieger.mx')

    def test_can_list_users(self):
        assert self.guard.list()

    def test_can_read_user_with_key(self):
        assert self.guard.read(other_key=self.other)

    @unittest.expectedFailure
    def test_can_read_user_with_email(self):
        assert self.guard.read(email='master@krieger.mx')

    def test_can_read_me(self):
        assert self.guard.read_me()

    def test_can_delete(self):
        to_delete = User(email='to-delete@krieger.mx').put().urlsafe()
        assert self.guard.delete(to_delete)

    def test_can_load_with_email(self):
        assert MyUserGuard.load_with_email('hola@krieger.mx')

    def test_can_load_with_token(self):
        token, user = self.guard.create_account(email='dude2@krieger.mx')
        assert MyUserGuard.load_with_token(token, UserStatus.Pending,
                                           TokenType.SignUp)

    def test_cannot_load_with_token_if_incorrect_token_type(self):
        pass

    def test_cannot_load_with_token_if_unexpected_user_status(self):
        token, user = self.guard.create_account(email='dude2@krieger.mx')
        MyUserGuard.set_password_with_signup_token(token, 'password')

        # TODO: check message?
        with self.assertRaises(InternalServerError):
            MyUserGuard.load_with_token(token, UserStatus.Pending,
                                        TokenType.SignUp)

    def test_cannot_load_with_token_if_user_does_not_exist(self):
        pass

    def test_can_set_password_with_signup_token(self):
        token, user = self.guard.create_account(email='dude3@krieger.mx')
        assert MyUserGuard.set_password_with_signup_token(token, 'password')

    def test_cannot_set_password_with_signup_token_if_short_password(self):
        pass

    def test_cannot_set_password_with_signup_token_user_does_not_exist(self):
        pass

    def test_cannot_set_password_with_signup_token_if_incorrect_token_type(self):
        pass

    def test_cannot_set_password_with_signup_token_if_user_is_not_pending(self):
        pass

    def test_can_load_with_login(self):
        token, user = self.guard.create_account(email='dude4@krieger.mx')
        MyUserGuard.set_password_with_signup_token(token, 'password')
        assert MyUserGuard.load_with_login(email='dude4@krieger.mx',
                                           password='password')

    def test_can_update_me(self):
        assert self.guard.update_me(email='new-email@krieger.mx')

    def test_can_update(self):
        assert self.guard.update(self.other, email='new-email@krieger.mx')

    def test_cannot_set_password_with_restore_token_user_does_not_exist(self):
        pass

    def test_cannot_set_password_with_restore_token_if_incorrect_token_type(self):
        pass

    def test_cannot_set_password_with_restore_token_if_user_is_not_active(self):
        pass
