Mixins
======

Some useful mixins for the datastore

.. automodule:: krieger_gae.ndb.mixins
    :members: