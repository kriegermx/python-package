from protorpc import remote
from krieger_gae.api import auth_required
import endpoints


class MyAPI(remote.Service):

    @endpoints.method(RESOURCE, Response, path='{key}', http_method='GET',
                      name='read')
    @auth_required
    def read(self, request, current_user):
        # do stuff
        return Response()
