from copy import copy

from google.appengine.ext import ndb

from .helpers import _urlsafe2key


class DatastoreModelIntrospector(object):
    """Helper class for model instrospection

    Parameters
    ----------
    object:  ndb.Model
        The model to be introspected

    Notes
    -----
    * This class uses some private properties
    * It also uses the _properties private property for ndb models, see section
    'Using Properties Named by String' in
    https://cloud.google.com/appengine/docs/python/ndb/queries
    """
    def __init__(self, model):
        self.model = model
        self._properties = self.model.__dict__['_properties']

    def _is_required(self, prop):
        try:
            return prop.__dict__['_required']
        except KeyError:
            return False

    def _is_indexed(self, prop):
        try:
            return prop.__dict__['_indexed']
        except KeyError:
            return False

    def _is_repeated(self, prop):
        try:
            return prop.__dict__['_repeated']
        except KeyError:
            return False

    def properties(self):
        """Returns the set of properties
        """
        return set([n for n, p in self._properties.items()])

    def key_properties(self):
        """Returns the set of key properties
        """
        return set([n for n, p in self._properties.items()
                    if isinstance(p, ndb.KeyProperty)])

    def blob_key_properties(self):
        """Returns the set of blob key properties
        """
        return set([n for n, p in self._properties.items()
                    if isinstance(p, ndb.BlobKeyProperty)])

    def structured_properties(self):
        """Returns the set of structured properties
        """
        return set([n for n, p in self._properties.items()
                    if isinstance(p, ndb.StructuredProperty)])

    def indexed_properties(self):
        """Returns the set of indexed properties
        """
        return set([n for n, p in self._properties.items()
                    if self._is_indexed(p)])

    def repeated_properties(self):
        """Returns the set of repeated properties
        """
        return set([n for n, p in self._properties.items()
                    if self._is_repeated(p)])

    def required_properties(self):
        """Returns the set of required properties
        """
        return set([n for n, p in self._properties.items()
                    if self._is_required(p)])

    def type_for_property(self, prop):
        """Returns the type of a given property
        """
        return self._properties[prop].__class__

    def get_by_name(self, prop):
        """Returns the property object given a property name
        """
        return self._properties[prop]

    def class_for_structured_property(self, prop):
        """
        Given a structured property name returns the class associated with it

        Notes
        -----
        https://cloud.google.com/appengine/docs/python/ndb/modelclass
        """
        cls_name = self.get_by_name(prop).__dict__['_modelclass'].__name__
        return ndb.Model._lookup_model(cls_name)

    def parse_urlsafe_from_dict(self, d):
        d = copy(d)
        keys = self.key_properties()

        for k, v in d.items():
            if k in keys:
                d[k] = _urlsafe2key(v)

        return d
