import unittest

from krieger_gae.ndb.parsers import json_loads, to_datetime, to_date
from krieger_gae.exceptions import BadRequest


class TestInput(unittest.TestCase):

    def test_can_parse_json(self):
        assert json_loads('{"key": "this is a valid json string"}')

    def test_parsing_invalid_json_raises_appropriate_error(self):
        with self.assertRaises(BadRequest):
            json_loads('not a valid json string')

    def test_invalid_datetime_string_raises_appropriate_error(self):
        with self.assertRaises(ValueError):
            to_datetime('not a valid datetime string')

    def test_invalid_datet_string_raises_appropriate_error(self):
        with self.assertRaises(ValueError):
            to_date('not a valid date string')
