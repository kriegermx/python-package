Constants
=========

The package provides a command line interface to generate constants for
Python, Javascript, Swift and Java from a yaml file (requires `click` to
be installed):

To follow this example move to `examples/constants` folder.

Suppose you have the following `constants.yaml` file:

.. literalinclude:: ../examples/constants/constants.yaml

The top level key is a group identifier, this will usually be an object
in your database.

The second key is the constant name, inside that key there is a list of
constants.

With the following locales files:

.. literalinclude:: ../examples/constants/locales/es.yaml

.. literalinclude:: ../examples/constants/locales/en.yaml

Python constant generation
--------------------------

Using `constants.yaml` you can run:

.. code-block:: shell

    constants python . ./python_out ./python_out

To generate:

constants.py

.. literalinclude:: ../examples/constants/python_out/constants.py

messages.pot

.. literalinclude:: ../examples/constants/python_out/messages.pot

translations/es/LC_MESSAGES/messages.po

.. literalinclude:: ../examples/constants/python_out/translations/es/LC_MESSAGES/messages.po

translations/en/LC_MESSAGES/messages.po

.. literalinclude:: ../examples/constants/python_out/translations/en/LC_MESSAGES/messages.po


For options run:

.. code-block:: shell

    constants python --help

Javascript constant generation
-------------------------------

Using `constants.yaml` you can run:

.. code-block:: shell

    constants javascript . ./js_out ./js_out

To generate:

constants.js

.. literalinclude:: ../examples/constants/js_out/constants.js

es.json

.. literalinclude:: ../examples/constants/js_out/es.json

en.json

.. literalinclude:: ../examples/constants/js_out/en.json

For options run:

.. code-block:: shell

    constants javascript --help