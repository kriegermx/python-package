import httplib
import endpoints


"""
Defines endpoints-compatible exceptions
"""


class BaseException(endpoints.ServiceException):
    """Base exception compatible with Google Cloud Endpoints
    """
    http_status = None

    def __init__(self, message=None, user_message=None):
        super(BaseException, self).__init__(message)
        self.user_message = user_message

    def to_dict(self):
        rv = {}
        rv['message'] = self.message
        rv['user_message'] = self.user_message
        return rv


class NotFound(BaseException):
    """Raise this error when source resource was not found
    """
    http_status = httplib.NOT_FOUND


class Forbidden(BaseException):
    """
    Raise this error when the user does not have permission to perform
    certain action
    See this: See this: http://es.slideshare.net/stormpath/secure-your-rest-api-the-right-way
    # noqa
    """
    http_status = httplib.FORBIDDEN


class Unauthorized(BaseException):
    """
    Raise this error when the user is not authenticated
    See this: http://es.slideshare.net/stormpath/secure-your-rest-api-the-right-way
    # noqa
    """
    http_status = httplib.UNAUTHORIZED


class BadRequest(BaseException):
    """
    Raise this when the user input is not in the right format
    """
    http_status = httplib.BAD_REQUEST


class InternalServerError(BaseException):
    """
    Raise this when something is wrong in the server
    """
    http_status = httplib.INTERNAL_SERVER_ERROR
