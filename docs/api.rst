Endpoints
=========

This module provides some utilities for integrating cookie-based authentication
(flask-login) with Google Cloud Endpoints

.. automodule:: krieger_gae.api
    :members: