from .guard import PostGuard, MyUserGuard
from .models import Post
from krieger_gae.ndb import ObjectGuard

# Bootstrap a dummy account, in your app whis will probably be an admin account
# created from the interactive console, create the rest of the accounts
# using create_account
userguard = MyUserGuard.bootstrap_account(password='mypassword',
                                          email='myemail@krieger.mx')

# Get the data for the current user
userguard.read_me()


# Instantiate a PostGuard
postguard = PostGuard(userguard)

# Create a new post
post = postguard.create(content='Content', tag=['tag1', 'tag2'],
                        user=userguard.key)
post_key = post.key

# Read the post whose key is post_key. post_key can be either a Key
# or a urlsafe key object
postguard.read(post_key)

# You can also instantiate a PostGuard like this
another_postguard = ObjectGuard(userguard, model=Post)
