from .notifier.notifier import Notifier

__version__ = "0.10"

_settings = dict()


def set_token_secret(token_secret):
    _settings['TOKEN_SECRET'] = token_secret


def set_flask_secret_key(secret_key):
    _settings['FLASK_SECRET_KEY'] = secret_key


def settings(key):
    return _settings.get(key)
