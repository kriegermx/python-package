from .dummy import User, Model, Guard, ModelWithRequired

from krieger_gae.exceptions import BadRequest, NotFound
from krieger_gae.ndb.ObjectGuard import ObjectGuard
from krieger_gae.testing import SingleDatastoreTestCase

from mock import MagicMock


class ObjectGuardTesting(SingleDatastoreTestCase):

    @classmethod
    def setUpClass(cls):
        super(ObjectGuardTesting, cls).setUpClass()

        user = User(email='hola@krieger.mx')
        user.put()

        cls.overlord = MagicMock()
        cls.overlord._user = user

        Model(prop1=1).put()
        Model(prop1=2).put()
        cls.a_key = Model(prop1=3).put()

        cls.guard = Guard(cls.overlord)

    def test_objectguard_rejects_if_prop_doesnt_exist_when_creating(self):
        with self.assertRaises(BadRequest):
            self.guard.create(not_a_prop=100)

    def test_objectguard_rejects_if_required_prop_doesnt_exist_when_creating(self):
        with self.assertRaises(BadRequest):
            ObjectGuard(self.overlord,
                        model=ModelWithRequired).create(prop1=10)

    def test_objectguard_rejects_if_prop_doesnt_exist_when_updating(self):
        with self.assertRaises(BadRequest):
            self.guard.update(self.a_key, payyyment_type='stuff')

    def test_can_list_a_specific_number_of_items_using_count(self):
        assert 1 == len(self.guard.list(count=1))

    def test_can_offset_a_specific_number_of_items_using_cursor(self):
        first = self.guard.list(count=1)[0]
        second = self.guard.list(count=1, offset=1)[0]
        assert first.key != second.key

    def test_use_cursors_in_list(self):
        res, cursor, more = self.guard.list(get_cursor=True)
        assert cursor

    def test_can_subselect_elements_using_filter(self):
        subset = self.guard.list(filter_=(Model.prop1 == 1,))
        assert all([e.prop1 == 1 for e in subset])

    def test_can_order_results_ascending(self):
        res = self.guard.list(order=(Model.prop1,))
        types = [e.prop1 for e in res]
        types_asc = sorted(types)
        assert types == types_asc

    def test_can_order_results_descending(self):
        res = self.guard.list(order=(-Model.prop1,))
        types = [e.prop1 for e in res]
        types_desc = sorted(types, reverse=True)
        assert types == types_desc

    def test_can_append_to_repeated_property(self):
        g = Guard(self.overlord, check_permissions=False)
        obj = g.create(rep1=[0, 1, 2, 3])
        new = g.update(obj.key, rep1=[4], append=True)
        assert set(new.rep1) == {0, 1, 2, 3, 4}

    def test_can_replace_to_repeated_property(self):
        original = [0, 1, 2, 3]
        g = Guard(self.overlord, check_permissions=False)
        obj = g.create(rep1=original)
        new = g.update(obj.key, rep1=[4], append=False)
        assert set(new.rep1) == {4}

    def test_can_create_many_objects(self):
        g = Guard(self.overlord, check_permissions=False)
        data = [dict(rep1=[1]), dict(rep1=[2]), dict(rep1=[3])]
        objects = g.create_multi(data)
        assert objects

    def test_can_delete_object(self):
        g = Guard(self.overlord, check_permissions=False)
        obj = g.create(prop1=1)
        g.delete(obj.key)

        with self.assertRaises(NotFound):
            g.read(obj.key)

    def test_can_delete_multi(self):
        g = Guard(self.overlord, check_permissions=False)
        data = [dict(rep1=[1]), dict(rep1=[2]), dict(rep1=[3])]
        objects = [o.key for o in g.create_multi(data)]

        g.delete(objects)
