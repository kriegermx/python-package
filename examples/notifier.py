from krieger_gae import Notifier
from google.appengine.ext import ndb


class Notification(ndb.Model):
    """
    Paramameters
    ------------
    title: str, optional
        Notification's title
    content: str, optional
        Notification's content
    resource: Key, optional
        Object associated with the notification, it's used for reference
        and to generate the url associated with the notification
    extra: dict, optional
        Metadata associated with the notification
    date: datetime, optional
        Timestamp, defaults to current datetime
    read: bool, optional
        Whether the notification has been read, defaults to false
    """
    title = ndb.StringProperty()
    content = ndb.StringProperty()
    resource = ndb.KeyProperty()
    extra = ndb.JsonProperty()
    date = ndb.DateTimeProperty(auto_now_add=True)
    read = ndb.BooleanProperty(default=False)

    def to_dict(self):
        d = super(Notification, self).to_dict()
        d['key'] = self.key.urslsafe()
        return d
