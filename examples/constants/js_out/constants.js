IncidentStatus = {
    Waiting: 'waiting',
    Accepted: 'accepted',
    Rejected: 'rejected',
}

IncidentType = {
    Medical: 'medical',
    Car: 'car',
    Individual: 'individual',
}

